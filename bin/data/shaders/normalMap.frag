#version 330

in vec2 vertexTexCoord;
in vec3 vpos;
in vec3 vertexnormal;
uniform sampler2DRect colorTex;
uniform vec3 lightpos;
uniform float specularness;
out vec4 outputColor;
void main( ) {
    vec3 N = normalize(cross(dFdy(vpos), dFdx(vpos)));
    // N is the world normal
    vec3 V = normalize(vertexnormal);
    vec3 R = reflect(V, N);
    vec3 L = normalize(vec3(lightpos.x, lightpos.y, lightpos.y));
    
    vec4 texture = texture( colorTex, vertexTexCoord ) * 0.7;
    vec4 ambient = vec4(0.2, 0.2, 0.2, 0.0);
    vec4 diffuse = vec4(0.2, 0.2, 0.2, 0.5) * max(dot(L, N), 0.0);
    vec4 specular = vec4(1.0, 1.0, 1.0, 0.8) * pow(max(dot(R, L), 0.0), specularness);
    outputColor = texture + ambient + diffuse + specular;
}

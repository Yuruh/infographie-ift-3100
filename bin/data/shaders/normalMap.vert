#version 330

uniform sampler2DRect grayTex;
uniform float intensity;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;

in vec2 texcoord;
in vec4 position;
in vec3 normal;

out vec3 vpos;
out vec3 vertexnormal;
out vec2 vertexTexCoord;

void main() {
    vec4 color = texture(grayTex, texcoord);
    vec4 pos = modelViewProjectionMatrix * vec4(position.x, position.y, color.x * intensity, position.w);
    vpos = pos.xyz;
    vertexTexCoord = texcoord;
    vertexnormal = (pos).xyz;
    gl_Position = pos;
}

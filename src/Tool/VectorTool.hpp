//
//  VectorTool.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-28.
//

#ifndef VectorTool_hpp
#define VectorTool_hpp

#include "ShapeTool.hpp"

class ofApp;

template<class S, class P>
class VectorTool : public ShapeTool<S, P> {
public:
    VectorTool(ofApp *app) : ShapeTool<S, P>::ShapeTool(app) {}
    AVector   *getCursorShape() { return &(ShapeTool<S, P>::shape); }
};

#endif /* VectorTool_hpp */

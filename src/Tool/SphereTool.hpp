#pragma once

#include "ShapeTool.hpp"

class ofApp;

template<class S, class P>
class SphereTool : public ShapeTool<S, P> {
public:
    SphereTool(ofApp *app) : ShapeTool<S, P>::ShapeTool(app) {}
    void    useTool(IDrawObserver *obs) {

    if (ShapeTool<S, P>::shape.isRaytraced())
      obs->addRaytraceable(new S(ShapeTool<S, P>::shape));
    else
      obs->addDrawable(new S(ShapeTool<S, P>::shape));
    }
};

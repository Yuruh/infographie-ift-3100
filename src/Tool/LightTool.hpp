//
//  LightTool.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-28.
//

#ifndef LightTool_hpp
#define LightTool_hpp

#include "ShapeTool.hpp"

class ofApp;

template<class S, class P>
class LightTool : public ShapeTool<S, P> {
    public:
        LightTool(ofApp *app) : ShapeTool<S, P>::ShapeTool(app) {}
        void    preDrawPreview() { ShapeTool<S, P>::shape.getLight()->enable(); }
        void    postDrawPreview() { ShapeTool<S, P>::shape.getLight()->disable(); }
        void    useTool(IDrawObserver *obs) { obs->addLight(new S(ShapeTool<S, P>::shape)); }
};

#endif /* LightTool_hpp */

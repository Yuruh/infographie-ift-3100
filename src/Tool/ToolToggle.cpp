//
//  ToolToggle.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-16.
//

#include "ToolToggle.hpp"

ToolToggle::ToolToggle(DrawingTool *vectorTool) : tool(vectorTool) {}

ToolToggle::ToolToggle(ToolToggle &other) {
    tool = other.getTool();
}

DrawingTool   *ToolToggle::getTool() {
    return this->tool;
}

//
//  ToolToggle.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-16.
//

#ifndef ToolToggle_hpp
#define ToolToggle_hpp

#include <stdio.h>
#include "ofxToggle.h"
#include "DrawingTool.hpp"

class ToolToggle : public ofxToggle {
    public:
        ToolToggle(ToolToggle &other);
        ToolToggle(DrawingTool*);
        void mouseEntered(int, int);
        DrawingTool   *getTool();
    private:
        DrawingTool   *tool;
};

#endif /* ToolToggle_hpp */

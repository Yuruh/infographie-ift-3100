//
//  ShapeTool.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-16.
//

#ifndef ShapeTool_hpp
#define ShapeTool_hpp

#include <stdio.h>
#include "ofxGui.h"
#include "IDrawable.hpp"
#include "DrawingTool.hpp"

class ofApp;

template<class S, class P>
class ShapeTool : public DrawingTool {
    public:
        ShapeTool(ofApp *app) : shape(), panel(app, &shape) {}
        virtual AVector   *getCursorShape() { return NULL; }
        virtual void    updatePreview(float time) {}
        virtual void    preDrawPreview() {}
        virtual void    postDrawPreview() { shape.draw(); }
        virtual void    useTool(IDrawObserver *obs) { obs->addDrawable(new S(shape)); }
        ToolPanel   &getPanel() { return panel; }
    protected:
        S           shape;
        P           panel;
};

#endif /* VectorTool_hpp */

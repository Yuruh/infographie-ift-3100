//
//  DrawingTool.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef DrawingTool_hpp
#define DrawingTool_hpp

#include "AVector.h"
#include "ToolPanel.hpp"
#include "IDrawObserver.hpp"

class DrawingTool {
public:
    virtual AVector   *getCursorShape() = 0;
    virtual void    preDrawPreview() = 0;
    virtual void    postDrawPreview() = 0;
    virtual void    useTool(IDrawObserver*) = 0;
    virtual void    updatePreview(float) = 0;
    virtual ToolPanel   &getPanel() = 0;
};

#endif /* DrawingTool_hpp */

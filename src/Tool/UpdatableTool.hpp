//
//  UpdatableTool.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-28.
//

#ifndef UpdatableTool_hpp
#define UpdatableTool_hpp

#include "ShapeTool.hpp"

class ofApp;

template<class S, class P>
class UpdatableTool : public ShapeTool<S, P> {
    public:
        UpdatableTool(ofApp *app) : ShapeTool<S, P>::ShapeTool(app) {}
        void    updatePreview(float time) { ShapeTool<S, P>::shape.update(time); }
        virtual void    useTool(IDrawObserver *obs) { obs->addUpdatable(new S(ShapeTool<S, P>::shape)); }
};

#endif /* UpdatableTool_hpp */

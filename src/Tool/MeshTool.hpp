//
//  MeshTool.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-30.
//

#ifndef MeshTool_hpp
#define MeshTool_hpp

#include "UpdatableTool.hpp"

class ofApp;

template<class S, class P>
class MeshTool : public UpdatableTool<S, P> {
public:
    MeshTool(ofApp *app) : UpdatableTool<S, P>::UpdatableTool(app) {}
    void    useTool(IDrawObserver *obs) {
        if (ShapeTool<S, P>::shape.isLoaded())
            obs->addUpdatable(new S(ShapeTool<S, P>::shape));
    }
};

#endif /* MeshTool_hpp */

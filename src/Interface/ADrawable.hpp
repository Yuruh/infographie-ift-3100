#pragma once

#include "IDrawable.hpp"
#include "ofxGui.h"
#include <stack>

class ADrawable : public IDrawable {
public:
    virtual void draw() = 0;
    virtual bool isSelected();
    //        virtual void revert();

    virtual ofVec3f get3DPosition() const = 0;

    virtual ofxToggle *getToggle();
    virtual void onToggle(const void *sender, bool &toggled);

    ADrawable();
    ADrawable(const std::string&);


protected:
    ofxToggle  selected;
    //        std::stack<ofMatrix4x4>  prevStates;

private:
    static unsigned int itemCount;
};

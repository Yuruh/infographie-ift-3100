//
// IDrawable.hpp for  in /home/yuruh/ulaval/infographie-ift-3100/src/Interface/
//
// Made by Antoine Lempereur
// Login   <lemper_a@epitech.eu>
//
// Started on  Sat Mar 10 14:16:22 2018 Antoine Lempereur
// Last update Fri Apr 20 13:55:36 2018 Antoine Lempereur
//

#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class IDrawable {
    public:
        virtual void draw() = 0;
        virtual bool isSelected() = 0;
        virtual ofxToggle *getToggle() = 0;
        virtual void onToggle(const void *sender, bool &toggled) = 0;
        virtual ofVec3f get3DPosition() const = 0;
};

#include "ADrawable.hpp"

unsigned int ADrawable::itemCount = 0;

ADrawable::ADrawable()
{
    this->selected.setup("Object " + std::to_string(itemCount - 4), false);
    this->selected.addListener(this, &ADrawable::onToggle);
    itemCount++;
}

ADrawable::ADrawable(const std::string &name)
{
    this->selected.setup(name, false);
    this->selected.addListener(this, &ADrawable::onToggle);
}

void ADrawable::onToggle(const void *sender, bool &toggled) {
  //might not be useful since user can check the state with isSelected()
}

bool  ADrawable::isSelected()
{
  return (this->selected);
}

ofxToggle *ADrawable::getToggle()
{
  return (&this->selected);
}

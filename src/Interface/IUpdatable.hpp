//
//  AVector.h
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-14.
//

#pragma once

#include "ofMain.h"
#include "ADrawable.hpp"

class IUpdatable : public ADrawable {
    public:
        virtual void update(float) = 0;
};

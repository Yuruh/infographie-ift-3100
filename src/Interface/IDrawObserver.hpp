//
//  IDrawObserver.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-28.
//

#pragma once

#include "IUpdatable.hpp"
#include "IDrawable.hpp"
#include "Light.hpp"
#include "ARaytraceable.hpp"

class IDrawObserver {
public:
    virtual void addDrawable(IDrawable*) = 0;
    virtual void addUpdatable(IUpdatable*) = 0;
    virtual void addRaytraceable(ARaytraceable*) = 0;
    virtual void addLight(Light*) = 0;
};

//
//  AGeometry.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef AGeometry_hpp
#define AGeometry_hpp

#include "ADrawable.hpp"

class AGeometry : public ADrawable {
    public:
        AGeometry();
        AGeometry(const AGeometry&);
        virtual void draw() = 0;
        ofMaterial  getMaterial() const;
        virtual void    setPosition(const ofVec3f &p);
        void        setColors(const ofColor &, const ofColor&, const ofColor&, const ofColor&);
        void        setShininess(const float &val);
        ofVec3f     get3DPosition() const;
        ofColor     getDiffuseColor() const;
        ofColor     getAmbientColor() const;
        ofColor     getSpecularColor() const;
        ofColor     getEmissiveColor() const;
        float       getShininess() const;
    protected:
        ofVec3f     pos;
        ofColor     color;
        ofMaterial  material;
};

#endif /* AGeometry_hpp */

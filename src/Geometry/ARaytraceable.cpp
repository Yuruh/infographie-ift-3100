#include "ARaytraceable.hpp"

ARaytraceable::ARaytraceable(const ARaytraceable &other) : AGeometry(other)
{
  this->cameraPos = ofVec3f(-600, 0, 0);
  this->reflectCoeff = other.reflectCoeff;
  this->refractCoeff = other.refractCoeff;
}

ARaytraceable::ARaytraceable(float refractCoeff, float reflectCoeff) : reflectCoeff(reflectCoeff), refractCoeff(refractCoeff)
{
}

void ARaytraceable::draw()
{
//  result.draw(-ofGetWidth() / 2, -ofGetHeight() / 2);
}

void ARaytraceable::setCameraPos(const ofVec3f &cameraPos)
{
  this->cameraPos = cameraPos;
}

void ARaytraceable::raytrace(bool doRaytrace) {
    this->raytraced = doRaytrace;
}

bool ARaytraceable::isRaytraced() const {
  return this->raytraced;
}

void ARaytraceable::setOtherRaytraceableObjects(std::vector<ARaytraceable*> *others) {
  this->otherRaytraceable = others;
}

ARaytraceable *ARaytraceable::getIntersectedObject(const Ray &ray, ARaytraceable *previouslyImpacted, double &distance)
{
  ARaytraceable *impacted = NULL;
  distance = INFINITY;

  for (auto elem: *(this->otherRaytraceable))
  {
    if (previouslyImpacted != elem)
    {
      double currentDistance = elem->rayIntersection(ray);
      if (currentDistance > EPSILON && currentDistance < distance)
      {
        distance = currentDistance;
        impacted = elem;
      }
    }
  }
  return impacted;
}

void ARaytraceable::compute(ofImage &result)
{
  result.getPixels().set(0);

  for (int i = 0; i < ofGetWidth(); i++) {
    for (int j = 0; j < ofGetHeight(); j++) {

      double minDist = INFINITY;
      Ray ray(i, j, this->cameraPos);
      ARaytraceable *impacted = this->getIntersectedObject(ray, NULL, minDist);

      if (impacted != NULL)
      {
//        std::cout << "ALLO" << std::endl;
          ofVec3f intersection = ray.getIntersection(minDist);
          ofVec3f normal = impacted->getNormal(intersection);
          float angle = ray.dir.dot(normal);
          if (normal.dot(ray.dir) < 0)
            normal *= -1;

          ofColor color = this->computeColor(impacted, intersection, normal);

          if (impacted->reflectCoeff > EPSILON)
          {
            Ray reflectedRay(intersection, (ray.dir - (-2 * (angle * normal))));

            ofColor reflectedColor = this->computeAlternativeRay(reflectedRay, impacted);

            color = reflectedColor * impacted->reflectCoeff + color * (1 - impacted->reflectCoeff);
          }
          else if (impacted->refractCoeff > EPSILON) //we forbid reflection + refraction
          {
            float indice = 1 / 1.33;
            float factor = indice * angle - sqrt(1 + (indice * indice) * ((angle * angle) - 1));
            Ray refractedRay(intersection, (indice * ray.dir + factor * normal) * -1);
            ofColor refractedColor = this->computeAlternativeRay(refractedRay, impacted);
            color = refractedColor * impacted->refractCoeff + color * (1 - impacted->refractCoeff);
          }

          result.getPixels().setColor(i, j, color);
      }
    }
  }
  result.update();
}

ofColor  ARaytraceable::computeAlternativeRay(const Ray &ray, ARaytraceable *impacted)
{
  double distance;
  ARaytraceable *object = this->getIntersectedObject(ray, impacted, distance);
  ofColor color(0, 0, 0, 255);

  if (object != NULL)
  {
    ofVec3f intersection = ray.getIntersection(distance);
    ofVec3f normal = object->getNormal(intersection);
    if (normal.dot(ray.dir) < 0)
      normal *= -1;

    color = this->computeColor(object, intersection, normal);
  }

  return color;
}

ofColor  ARaytraceable::computeColor(ARaytraceable* impacted, ofVec3f intersection, ofVec3f normal)
{
  std::vector<ofVec3f> lightsPos;
  lightsPos.push_back(ofVec3f(0, 0, 500));
//  lightsPos.push_back(ofVec3f(500, 500, 0));
  lightsPos.push_back(ofVec3f(0, 500, -500));
  //ofVec3f light(0, 0, 500);
  ofColor color = impacted->getDiffuseColor();
  ofColor finalColor(0, 0, 0);
  float angle = 0;

//  for (auto light : *(this->lights))
  for (auto light : lightsPos)
  {
//    std::cout << light->get3DPosition() << std::endl;
    ofVec3f dirLight = intersection - light;
    dirLight = dirLight.normalize();
    angle += dirLight.dot(normal);
    if (angle < 1)
      finalColor += color * angle;
    if (finalColor.r > color.r)
      finalColor.r = color.r;
    if (finalColor.g > color.g)
      finalColor.g = color.g;
    if (finalColor.b > color.b)
      finalColor.b = color.b;
  }
  return finalColor;
}

void ARaytraceable::setReflection(float value)
{
    this->reflectCoeff = value;
}

void ARaytraceable::setRefraction(float value)
{
    this->refractCoeff = value;
}

void ARaytraceable::setLights(std::vector<Light*> *lights)
{
    this->lights = lights;
}

//
//  Box.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "Box.hpp"

Box::Box() {
    box.setWidth(200);
    box.setHeight(100);
    box.setDepth(200);
}

Box::Box(const Box &other) : AGeometry(other) {
    box.setWidth(other.getWidth());
    box.setHeight(other.getHeight());
    box.setDepth(other.getDepth());
}

void Box::draw() {
    material.begin();
    ofFill();
    box.setPosition(pos.x, pos.y, pos.y);
    if (isSelected()) box.drawWireframe();
    else box.draw();
    material.end();
}

float Box::getWidth() const {
    return box.getWidth();
}

float Box::getHeight() const {
    return box.getHeight();
}

float Box::getDepth() const {
    return box.getDepth();
}

void Box::setWidth(float w) {
    box.setWidth(w);
}

void Box::setHeight(float h) {
    box.setHeight(h);
}

void Box::setDepth(float d) {
    box.setDepth(d);
}

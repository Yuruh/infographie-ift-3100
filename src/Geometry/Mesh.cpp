//
// Created by yuruh on 15/02/18.
//

#include "Mesh.hpp"
#include "ofApp.h"

Mesh::Mesh() {
    this->setScale(ofVec3f(0.5, 0.5, 0.5));
    this->setRotation(ofVec3f(180, 180, 0));
    this->setPosition(ofVec3f(0, 0, 0));
}

Mesh::Mesh(const Mesh &other) {
    this->loadMesh(other.getName(), other.getFilePath());
    this->setPosition(other.get3DPosition());
    this->setScale(other.getScale());
    this->setRotation(other.getRotation());
}

bool    Mesh::isLoaded() {
    return !this->filepath.empty();
}

void Mesh::loadMesh(const std::string &name, const std::string &filepath) {
    ofSetLogLevel(OF_LOG_VERBOSE);

    this->name = name;
    this->filepath = filepath;

    glShadeModel (GL_SMOOTH);

    this->loadModel(filepath);
    this->getMeshBoundingBoxDimension();
    if (this->hasAnimations())
    {
        this->currentAnimation = 0;
        this->getAnimation(this->currentAnimation).play();
        this->getAnimation(this->currentAnimation).setLoopState(OF_LOOP_NORMAL); // OF_LOOP_PALINDROME is still TO_DO in the source code
    }
}

void Mesh::getMeshBoundingBoxDimension() {
    ofMesh mesh = this->getMesh(0);

    auto xExtremes = std::minmax_element(mesh.getVertices().begin(), mesh.getVertices().end(),
                                         [](const ofPoint& lhs, const ofPoint& rhs) {
                                             return lhs.x < rhs.x;
                                         });
    auto yExtremes = std::minmax_element(mesh.getVertices().begin(), mesh.getVertices().end(),
                                         [](const ofPoint& lhs, const ofPoint& rhs) {
                                             return lhs.y < rhs.y;
                                         });
    auto zExtremes = std::minmax_element(mesh.getVertices().begin(), mesh.getVertices().end(),
                                         [](const ofPoint& lhs, const ofPoint& rhs) {
                                             return lhs.z < rhs.z;
                                         });

    ofVec3f dim(xExtremes.second->x - xExtremes.first->x,
                yExtremes.second->y - yExtremes.first->y,
                zExtremes.second->z - zExtremes.first->z);
    this->boundingBox.set(dim.x, dim.y, dim.z);
}

void Mesh::draw() {
    ofEnableDepthTest();
    if (this->isSelected())
        this->drawWireframe();
    else
        this->drawFaces();
    ofDisableDepthTest();
}

void Mesh::update(float deltaTime)
{
    ofPushMatrix();
    this->getMeshBoundingBoxDimension();
    ofPopMatrix();
    this->boundingBox.setPosition(this->getPosition());

    if (this->hasAnimations())
    {
        ofxAssimpModelLoader::update();
        if (this->getAnimation(this->currentAnimation).getPosition() > 0.99)
        {
            this->currentAnimation = std::rand() % this->getAnimationCount();
            this->getAnimation(this->currentAnimation).play();
        }
    }
}

void Mesh::setScale(const ofVec3f &s) {
    ofxAssimpModelLoader::setScale(s.x, s.y, s.z);
    scale = s;
}

ofVec3f Mesh::getScale() const {
    return scale;
}

void Mesh::setRotation(const ofVec3f &rotation) {
    this->rotation = rotation;
    ofxAssimpModelLoader::setRotation(1, rotation.x, 1, 0, 0);
    ofxAssimpModelLoader::setRotation(2, rotation.y, 0, 1, 0);
    ofxAssimpModelLoader::setRotation(3, rotation.z, 0, 0, 1);
}

ofVec3f Mesh::getRotation() const {
    return rotation;
}

void Mesh::setPosition(const ofVec3f &p) {
    ofxAssimpModelLoader::setPosition(p.x, p.y, p.z);
    pos = this->getPosition();
}

ofVec3f Mesh::get3DPosition() const {
  return pos;
}

std::string Mesh::getFilePath() const {
    return filepath;
}

std::string Mesh::getName() const {
    return name;
}

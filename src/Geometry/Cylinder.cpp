#include "Cylinder.hpp"

Cylinder::Cylinder(double radius, float refractCoeff, float reflectCoeff)
  : ARaytraceable(refractCoeff, reflectCoeff),
    radius(radius)
{
}

Cylinder::Cylinder()
{
  this->radius = 20;
}

Cylinder::Cylinder(const Cylinder &other) : ARaytraceable(other)
{

}


double Cylinder::rayIntersection(const Ray &ray)
  {
    // distance de l'intersection la plus près si elle existe
    double distance;

    // distance du pointsize d'intersection
    double t;

    // vecteur entre le centre de la sphère et l'origine du rayon
    ofVec3f delta = pos - ray.origin;

    // calculer a
    double a = ray.dir.x * ray.dir.x + ray.dir.y * ray.dir.y;

    // calculer b
    double b = 2 * (delta.x * ray.dir.x + delta.y * ray.dir.y);

    // calculer c
    double c =  delta.x * delta.x + delta.y * delta.y - radius * radius;

    // calculer le discriminant de l'équation quadratique
    double discriminant = b * b - 4 * a * c;


    // valider si le discriminant est négatif
    if (discriminant < 0)
    {
      // il n'y a pas d'intersection avec cette sphère
      return distance = 0;
    }


    // calculer la racine carrée du discriminant seulement si non-négatif
    discriminant = sqrt(discriminant);
    double diviser = 2 * a;

    return (std::min((-b - discriminant) / diviser, (-b + discriminant) / diviser));

    // déterminer la distance de la première intersection
    t = b - discriminant;

    // valider si la distance de la première intersection est dans le seuil de tolérance
    if (t > EPSILON)
      distance = t;
    else
    {
      // déterminer la distance de la première intersection
      t = b + discriminant;

      // valider si la distance de la seconde intersection est dans le seuil de tolérance
      if (t > EPSILON)
        distance = t;
      else
        distance = 0;
    }

    // retourner la distance du point d'intersection
    return distance;
}

void  Cylinder::draw()
{
  ofDrawSphere(0, 0, this->radius);
}

ofVec3f Cylinder::getNormal(const ofVec3f &intersection)
{
  ofVec3f ret = (intersection - this->pos).normalize();
  ret.z = 0;
  return ret;
}

void Cylinder::setRadius(float value)
{
  this->radius = value;
}

float Cylinder::getRadius() const
{
  return this->radius;
}

//
//  Image.cpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 11/03/2018.
//

#include "Image.hpp"
#include "ofApp.h"

Image::Image() : image(NULL), color(ofColor(255, 255, 255)) {}

Image::Image(const Image &other) : ADrawable(other.getName()) {
    this->image = other.getImage();
    this->pos = other.getPos();
    this->size = other.getSize();
    this->color = other.getColor();
    this->name = other.getName();
}

void        Image::loadImage(const std::string &name, const std::string &filepath) {
    ofImage *old = NULL;
    
    if (image) old = image;
    
    image = new ofImage(filepath);
    this->name = name;
    size = ofVec2f(image->getWidth(), image->getHeight());
    
    while (size[0] > ofGetWidth() || size[1] > ofGetHeight()) {
        size /= 1.005;
    }
    initialSize = size;
    
    if (old) delete old;
}

void        Image::resetSize() {
    size = initialSize;
}

void        Image::draw()
{
    if (image) {
        ofSetColor(color);
        image->draw(pos[0]  - (size[0] / 2), pos[1] - (size[1] / 2), size[0], size[1]);

        if (this->isSelected())
        {
          ofNoFill();
          ofSetLineWidth(10);
          ofSetColor(155, 155, 155, 200);
          ofDrawRectangle(pos.x, pos.y, size.x, size.y);
        }
    }
}

void        Image::setSize(const ofVec2f &size) {
    this->size = size;
}

ofVec2f     Image::getSize() const {
    return size;
}

void        Image::setPos(const ofVec2f &pos) {
    this->pos = pos;
}

ofVec2f     Image::getPos() const {
    return pos;
}

ofVec3f     Image::get3DPosition() const {
  ofVec3f ret;

  ret.x = this->pos.x;
  ret.y = this->pos.y;
  ret.z = 0;
  return ret;
}

void        Image::setColor(const ofColor &color) {
    this->color = color;
}

ofColor     Image::getColor() const {
    return color;
}

std::string Image::getName() const {
    return name;
}

ofImage     *Image::getImage() const {
    return image;
}

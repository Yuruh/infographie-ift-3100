//
//  NormalMapping.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-29.
//

#include "NormalMapping.hpp"

const string NormalMapping::DEFAULT_FILE = "normal_mapping/default.jpg";

NormalMapping::NormalMapping() : lightpos(0, -0.4, 0.0) {
    intensity = 50;
    specularness = 40;
    shader.load( "shaders/normalmap");
    textureImage.load("normal_mapping/textures/colormap_result.png");
    loadMapFile(DEFAULT_FILE);
}

NormalMapping::NormalMapping(const NormalMapping &other) : AGeometry(other) {
    specularness = 40;
    this->shader = other.getShader();
    this->textureImage = other.getTextureImage();
    this->heightmap = other.getHeightMap();
    this->plane = other.getPlane();
    this->intensity = other.getIntensity();
    this->lightpos = other.getLightPosition();
}

void NormalMapping::draw() {
    shader.begin();
    shader.setUniformTexture( "grayTex", heightmap.getTexture(), 0 );
    shader.setUniformTexture( "colorTex", textureImage.getTexture(), 1 );
    shader.setUniform3f( "lightpos", lightpos );
    shader.setUniform1f( "specularness", specularness );
    shader.setUniform1f("intensity", intensity);
    plane.setPosition(pos);
    plane.draw();
    shader.end();
}

void NormalMapping::loadMapFile(const std::string &fileName) {
    heightmap.load(fileName);
    
    int planeWidth = heightmap.getWidth();
    int planeHeight = heightmap.getHeight();
    int planeGridSize = 1;
    int planeColumns = planeWidth / planeGridSize;
    int planeRows = planeHeight / planeGridSize;
    
    plane.set(planeWidth, planeHeight, planeColumns, planeRows, OF_PRIMITIVE_TRIANGLES);
    plane.mapTexCoords(0, 0, heightmap.getWidth(), heightmap.getHeight());
}

void NormalMapping::resetMapping() {
    loadMapFile(DEFAULT_FILE);
}

ofShader    NormalMapping::getShader() const {
    return shader;
}

ofImage     NormalMapping::getTextureImage() const {
    return textureImage;
}

ofImage     NormalMapping::getHeightMap() const {
    return heightmap;
}

ofPlanePrimitive    NormalMapping::getPlane() const {
    return plane;
}

void       NormalMapping::setIntensity(const float &nb) {
    intensity = nb;
}

float       NormalMapping::getIntensity() const {
    return intensity;
}

float       NormalMapping::getSpecularity() const {
    return specularness;
}

void        NormalMapping::setSpecularity(const float &nb) {
    specularness = nb;
}

ofVec3f     NormalMapping::getLightPosition() const {
    return lightpos;
}

void        NormalMapping::setLightPosition(const ofVec3f &vec) {
    lightpos = vec;
}


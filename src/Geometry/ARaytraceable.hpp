#pragma once

#define EPSILON 0.00001
//#define INFINITY  100000000

#include "ofMain.h"
#include "AGeometry.hpp"
#include "Light.hpp"
#include "Ray.hpp"

class ARaytraceable : public AGeometry
{
protected:
  float   reflectCoeff;
  float   refractCoeff;
  bool    raytraced;

  //dans l'idéal, j'ai accès à ça aussi :
  std::vector<ARaytraceable*> *otherRaytraceable;
  std::vector<Light*>         *lights;
  ofVec3f                     cameraPos;

public:
//  ARaytraceable();
  ARaytraceable(float refractCoeff = 0, float reflectCoeff = 0);
  ARaytraceable(const ARaytraceable&);
  virtual double rayIntersection(const Ray &ray) = 0;
  virtual ofVec3f getNormal(const ofVec3f &) = 0;
  virtual void draw();
  void compute(ofImage &result);
  virtual void raytrace(bool);
  bool isRaytraced() const;
  void setCameraPos(const ofVec3f &);
  void setOtherRaytraceableObjects(std::vector<ARaytraceable*> *others);
  void setLights(std::vector<Light*> *lights);

  ARaytraceable* getIntersectedObject(const Ray &ray, ARaytraceable *previouslyImpacted, double &distance);
  ofColor computeColor(ARaytraceable*, ofVec3f, ofVec3f);
  ofColor computeAlternativeRay(const Ray &ray, ARaytraceable *impacted);
  void setReflection(float);
  void setRefraction(float);

};

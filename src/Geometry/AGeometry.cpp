//
//  AGeometry.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "AGeometry.hpp"

AGeometry::AGeometry() {
    pos.x = 0;
    pos.y = 0;
    pos.z = 0;
}

AGeometry::AGeometry(const AGeometry &other) {
    this->pos = other.get3DPosition();
    this->material = other.getMaterial();
}

ofMaterial  AGeometry::getMaterial() const {
    return material;
}

void    AGeometry::setShininess(const float &val) {
    material.setShininess(val);
}

void    AGeometry::setPosition(const ofVec3f &p) {
    pos = p;
}

void    AGeometry::setColors(const ofColor& diffuse, const ofColor& ambient, const ofColor& specular, const ofColor& emissive) {
    material.setColors(diffuse, ambient, specular, emissive);
}

ofVec3f AGeometry::get3DPosition() const {
    return pos;
}

ofColor AGeometry::getDiffuseColor() const {
    return material.getDiffuseColor();
}

ofColor AGeometry::getAmbientColor() const {
    return material.getAmbientColor();
}

ofColor AGeometry::getSpecularColor() const {
    return material.getSpecularColor();
}

ofColor AGeometry::getEmissiveColor() const {
    return material.getEmissiveColor();
}

float   AGeometry::getShininess() const {
    return material.getShininess();
}

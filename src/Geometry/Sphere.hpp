//
//  Sphere.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef Sphere_hpp
#define Sphere_hpp

#include "ARaytraceable.hpp"

class Sphere : public ARaytraceable
{
    public:
        Sphere();
        Sphere(const Sphere&);
        Sphere(double radius, float refractCoeff = 0, float reflectCoeff = 0);
        void draw();
        float getRadius() const;
        void setRadius(float);
        double rayIntersection(const Ray &ray);
        ofVec3f getNormal(const ofVec3f &);

    protected:
        ofSpherePrimitive   sphere;
};

#endif /* Sphere_hpp */

#pragma once

#include "ARaytraceable.hpp"

class Cylinder : public ARaytraceable
{
protected:
  double radius;
public:
  Cylinder();
  Cylinder(const Cylinder &);
  Cylinder(double radius, float refractCoeff = 0, float reflectCoeff = 0);
  double rayIntersection(const Ray &ray);
  ofVec3f getNormal(const ofVec3f &);
  void draw();
  void setRadius(float);
  float getRadius() const;
};

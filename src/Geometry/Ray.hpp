#pragma once

#define	ANTI_FISH_EYE_VALUE	2.144505

#include "ofMain.h"

struct Ray {
  ofVec3f origin;
  ofVec3f dir;

  Ray(ofVec3f origin, ofVec3f dir)
  {
    this->origin = origin;
    this->dir = dir;
  }

  Ray(double i, double j, const ofVec3f &origin);

  ofVec3f getIntersection(double distance) const;
};

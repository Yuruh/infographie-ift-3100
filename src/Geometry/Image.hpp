//
//  Image.hpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 11/03/2018.
//

#ifndef Image_hpp
#define Image_hpp

#include <stdio.h>
#include "ADrawable.hpp"

class Image: public ADrawable, public ofImage
{
public:
    Image();
    Image(const Image&);

    void    loadImage(const std::string &name, const std::string &filepath);
    void    draw();
    void    resetSize();

    ofImage     *getImage() const;
    std::string getName() const;
    ofVec2f getSize() const;
    ofVec2f getPos() const ;
    ofVec3f get3DPosition() const;
    ofColor getColor() const;

    void    setSize(const ofVec2f&);
    void    setPos(const ofVec2f&);
    void    setColor(const ofColor&);
 
    // ImageToolPanel  *panel;

protected:
    ofImage     *image;

    std::string name;
    ofVec2f     size;
    ofVec2f     initialSize;
    ofVec2f     pos;
    ofColor     color;
};

#endif /* Image_hpp */

//
//  Light.cpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 23/04/2018.
//

#include "Light.hpp"
#include "ofApp.h"

unsigned int Light::lightCount = 1;

Light::Light() : ADrawable() {
    this->_light = new ofLight();
    this->directional = false;
    this->pointlight = true;
    this->spotlight = false;
}

Light::Light(const std::string &name) : ADrawable(name) {
    this->_light = new ofLight();
    this->directional = false;
    this->pointlight = true;
    this->spotlight = false;
}

Light::Light(const Light &other): ADrawable("Light " + std::to_string(this->lightCount)) {
    this->lightCount++;

    this->_light = new ofLight();
    this->_light->setPosition(other.getLight()->getPosition());
    this->_light->setOrientation(other.getLight()->getOrientationEuler());
    this->_light->setAttenuation(other.getAttenuation());

    this->directional = other.isDirectional();
    this->pointlight = other.isPointlight();
    this->spotlight = other.isSpotlight();

    this->_light->setAmbientColor(other.getLight()->getAmbientColor());
    this->_light->setDiffuseColor(other.getLight()->getDiffuseColor());
    this->_light->setSpecularColor(other.getLight()->getSpecularColor());

    if (this->directional) this->_light->setDirectional();
    if (this->pointlight) this->_light->setPointLight();
    if (this->spotlight) {
        this->_light->setSpotlight();
        this->_light->setSpotConcentration(other.getLight()->getSpotConcentration());
        this->_light->setSpotlightCutOff(other.getLight()->getSpotlightCutOff());
    }
}

void        Light::draw() {
     this->_light->enable();
}

// GETTER
ofVec3f     Light::get3DPosition() const {
    return this->_light->getPosition();
}

ofLight     *Light::getLight() const {
    return this->_light;
}

bool        Light::isDirectional() const {
    return this->directional;
}

bool        Light::isPointlight() const {
    return this->pointlight;
}

bool        Light::isSpotlight() const {
    return this->spotlight;
}

float       Light::getAttenuation() const {
    return this->attenuation;
}

//SETTER
void        Light::setLightDirectional(const bool &dir) {
    this->directional = dir;
}

void        Light::setLightPointlight(const bool &point) {
    this->pointlight = point;
}

void        Light::setLightSpotlight(const bool &spot) {
    this->spotlight = spot;
}

void        Light::setLightAttenuation(const float &attenuation) {
    this->attenuation = attenuation;
}

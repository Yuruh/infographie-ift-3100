#include "Ray.hpp"

Ray::Ray(double i, double j, const ofVec3f &origin)
{
  double	x;
  double	y;
//  double	z;
//  double fov = 90;

//  x = ofGetWidth() / 2 / tan(fov * M_PI / 180);
//  y = ofGetWidth() / 2 - i;
//  z = ofGetHeight() / 2 - j;

  // x = (2 * ((i + 0.5) / ofGetWidth()) - 1) * tan(fov / 2 * M_PI / 180) * (ofGetWidth() / ofGetHeight());
  // y = (1 - 2 * ((j + 0.5) / ofGetHeight())) * tan(fov / 2 * M_PI / 180);



  x = (i * 2 / ofGetWidth() - 1) * sin(60 / 2) * -1;
  y = (j * 2 / ofGetHeight() - 1) * sin(90 / 2) * -1;

  this->dir = ofVec3f(x, y, -1);
  //this->dir = ofVec3f(0, 0, 1);
  this->origin = origin;
  //this->direction.rotate(scene->getRotation());
  this->dir = this->dir.normalize();
//  std::cout << this->dir << std::endl;
}

ofVec3f Ray::getIntersection(double distance) const
{
  return this->origin + distance * this->dir;
}

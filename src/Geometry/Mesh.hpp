//
// Created by yuruh on 15/02/18.
//

#ifndef INFOGRAPHIE_IFT_3100_MESH_HPP
#define INFOGRAPHIE_IFT_3100_MESH_HPP

#include <string>
#include "IUpdatable.hpp"
#include "ofxAssimpModelLoader.h"

class Mesh : public IUpdatable, public ofxAssimpModelLoader
{
public:
    Mesh();
    Mesh(const Mesh &);

    void loadMesh(const std::string&, const std::string&);
    void draw();
    void update(float);
    void setPosition(const ofVec3f&);
    void setRotation(const ofVec3f&);
    void setScale(const ofVec3f&);

    ofVec3f get3DPosition() const;
    ofVec3f getRotation() const;
    ofVec3f getScale() const;
    std::string getName() const;
    std::string getFilePath() const;
    
    bool    isLoaded();

    //  ofVec3f boundingBox;
    ofBoxPrimitive  boundingBox;
private:
    std::string filepath;
    std::string name;
    void getMeshBoundingBoxDimension();
    ofVec3f pos;
    ofVec3f rotation;
    ofVec3f scale;
    int currentAnimation;
};

#endif //INFOGRAPHIE_IFT_3100_MESH_HPP

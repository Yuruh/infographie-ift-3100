//
//  NormalMapping.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-29.
//

#ifndef NormalMapping_hpp
#define NormalMapping_hpp

#include "AGeometry.hpp"

class NormalMapping : public AGeometry {
    public:
        NormalMapping();
        NormalMapping(const NormalMapping &other);
        void    draw();
        void    loadMapFile(const std::string&);
        ofShader    getShader() const;
        ofImage     getTextureImage() const;
        ofImage     getHeightMap() const;
        ofPlanePrimitive    getPlane() const;
        float       getIntensity() const;
        void        setIntensity(const float&);
        float       getSpecularity() const;
        void        setSpecularity(const float&);
        ofVec3f     getLightPosition() const;
        void        setLightPosition(const ofVec3f&);
        void        resetMapping();
    
    protected:
        ofShader        shader;
        ofPlanePrimitive plane;
    
        float           intensity;
        float           specularness;
        ofVec3f         lightpos;
    
        ofImage heightmap, textureImage;
    
    private:
        static const std::string DEFAULT_FILE;
};
#endif /* NormalMapping_hpp */

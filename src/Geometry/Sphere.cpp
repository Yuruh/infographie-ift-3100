//
//  Sphere.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "Sphere.hpp"

Sphere::Sphere() {
    sphere.setRadius(100);
    sphere.setResolution(500);
    this->raytraced = false;
}

Sphere::Sphere(const Sphere &other) : ARaytraceable(other) {
    sphere.setRadius(other.getRadius());
    sphere.setResolution(500);
    raytraced = other.raytraced;
}

Sphere::Sphere(double radius, float refractCoeff, float reflectCoeff)
  : ARaytraceable(refractCoeff, reflectCoeff)
{
  this->setRadius(radius);
}

void    Sphere::draw() {
  if (this->raytraced) {
    ARaytraceable::draw();
  }
  else
  {
    material.begin();
    ofFill();
    sphere.setPosition(pos.x, pos.y, pos.y);
    if (isSelected()) sphere.drawWireframe();
    else sphere.draw();
    material.end();
  }
}

float   Sphere::getRadius() const {
    return sphere.getRadius();
}

void    Sphere::setRadius(float r) {
    sphere.setRadius(r);
}

double Sphere::rayIntersection(const Ray &ray)
{
    // distance de l'intersection la plus près si elle existe
    double distance;

    // distance du pointsize d'intersection
    double t;

    // vecteur entre le centre de la sphère et l'origine du rayon
    ofVec3f delta = pos - ray.origin;

    // calculer a
    double a = delta.dot(delta);

    // calculer b
    double b = delta.dot(ray.dir);

    // calculer c
    double c = this->getRadius() * this->getRadius();

    // calculer le discriminant de l'équation quadratique
    double discriminant = b * b - a + c;


    // valider si le discriminant est négatif
    if (discriminant < 0)
    {
      // il n'y a pas d'intersection avec cette sphère
      return distance = 0;
    }

//    std::cout << "intersect ?" << std::endl;

    // calculer la racine carrée du discriminant seulement si non-négatif
    discriminant = sqrt(discriminant);

    // déterminer la distance de la première intersection
    t = b - discriminant;

    // valider si la distance de la première intersection est dans le seuil de tolérance
    if (t > EPSILON)
      distance = t;
    else
    {
      // déterminer la distance de la première intersection
      t = b + discriminant;

      // valider si la distance de la seconde intersection est dans le seuil de tolérance
      if (t > EPSILON)
        distance = t;
      else
        distance = 0;
    }

    // retourner la distance du point d'intersection
    return distance;
}

ofVec3f Sphere::getNormal(const ofVec3f &intersection)
{
  return (intersection - this->pos).normalize();
}

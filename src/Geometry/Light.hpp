//
//  Light.hpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 23/04/2018.
//

#ifndef Light_hpp
#define Light_hpp

#include <stdio.h>
#include "ADrawable.hpp"

class Light: public ADrawable
{
public:
    Light();
    Light(const Light&);
    Light(const std::string &name);
    
    void        draw();
    
    ofVec3f     get3DPosition() const;
    ofLight     *getLight() const;
    float       getAttenuation() const;
    bool        isDirectional() const;
    bool        isPointlight() const;
    bool        isSpotlight() const;
    
    void        setLightAttenuation(const float&);
    void        setLightDirectional(const bool&);
    void        setLightPointlight(const bool&);
    void        setLightSpotlight(const bool&);

protected:
    ofLight     *_light;

    static unsigned int lightCount;
    bool        directional;
    bool        pointlight;
    bool        spotlight;
    float       attenuation;
};

#endif /* Light_hpp */

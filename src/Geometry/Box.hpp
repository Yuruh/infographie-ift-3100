//
//  Box.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef Box_hpp
#define Box_hpp

#include "AGeometry.hpp"

class Box : public AGeometry {
    public:
        Box();
        Box(const Box &other);
        void    draw();
        float   getWidth() const;
        float   getHeight() const;
        float getDepth() const;
        void setWidth(float w);
        void setHeight(float h);
        void setDepth(float d);
    protected:
        ofBoxPrimitive  box;
        float width;
        float height;
        float depth;
};

#endif /* Box_hpp */

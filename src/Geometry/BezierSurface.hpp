//
//  BezierSurface.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-17.
//

#ifndef BezierSurface_hpp
#define BezierSurface_hpp

#include "AGeometry.hpp"
#include "ofVboMesh.h"
#include "ofMain.h"

class BezierSurface : public AGeometry {
public:
    BezierSurface();
    void draw();
    void drawWireframe();
    void drawControls();
    void reset();
    vector<ofVec3f> getVertices();
    void setVertices(vector<ofVec3f>&);
    vector<ofVec3f> getPoints();
    void    setControlPnts(vector<ofVec3f>&);
    void    setDimension(int dim);
    int     getDimension();
    void    setPoint(const ofVec3f&, int);
    void    createSurface();
    
    bool    showingWireFrame() const;
    bool    showingControls() const;
    void    setShowControls(bool);
    void    setShowWireFrame(bool);
    
    void    setPosition(const ofVec3f &p);
    
    void setSize(const ofVec2f &s);
    ofVec2f getSize() const;

private:
    ofVec2f size;
    int cx;
    int cy;
    int rx;
    int ry;
    vector<ofVec3f> points;
    vector<ofVec3f> verts;
    ofVboMesh   mesh;
    bool showControls;
    bool showWireFrame;
    
    void    calculateSurface(vector<ofVec3f>&, vector<ofVec3f>&, int, int, int, int);
    double  bezierBlend(int k, double mu, int n);
    void    loadDefaultPoints();
    void    setup(int dim, int res);
};

#endif /* BezierSurface_hpp */

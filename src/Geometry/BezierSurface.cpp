//
//  BezierSurface.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-17.
//

#include "BezierSurface.hpp"

BezierSurface::BezierSurface() {
    showWireFrame = true;
    showControls = false;
    size.x = 200;
    size.y = 200;
    setup(4, 20);
}

void BezierSurface::setup(int dim, int res) {
    cx = dim-1;
    cy = dim-1;
    rx = res;
    ry = res;

    int i,j;

    verts.clear();
    for (i=0; i<rx; i++) {
        for (j=0; j<ry; j++) {
            verts.push_back(ofVec3f(ofMap(i, 0, rx, 0, size.x), ofMap(j, 0, ry, 0, size.y), 0));
        }
    }

    loadDefaultPoints();

    mesh = ofMesh::plane(size.x, size.y, rx, ry, OF_PRIMITIVE_TRIANGLES);
    createSurface();

    vector<ofVec3f> vertices = mesh.getVertices();
    for (i=0; i < vertices.size(); i++) {
        mesh.setTexCoord(i, ofVec2f(vertices[i].x, vertices[i].y));
    }
}

void BezierSurface::draw() {
    ofSetColor(color);
    if (showControls)
        drawControls();
    material.begin();
    if (this->isSelected() || showWireFrame)
        mesh.drawWireframe();
    else
        mesh.draw();
    material.end();
}

void BezierSurface::drawWireframe() {
    mesh.drawWireframe();
}

void BezierSurface::drawControls() {
    ofSetColor(200,200,200);
    for (ofVec3f vec : points) {
        ofFill();
        ofDrawSphere(vec.x + pos.x, vec.y + pos.y, vec.z + pos.z, 5);
    }
}

vector<ofVec3f> BezierSurface::getVertices(){
    return mesh.getVertices();
}

void BezierSurface::setVertices(vector<ofVec3f> & verts){
    for (int i=0; i<verts.size(); i++) {
        mesh.setVertex(i, verts[i]);
    }
}

vector<ofVec3f> BezierSurface::getPoints(){
    return points;
}

void BezierSurface::setControlPnts(vector<ofVec3f> &vec){
    points = vec;
    createSurface();
}

void BezierSurface::setPoint(const ofVec3f &p, int position) {
    points[position] = p;
}

void BezierSurface::setSize(const ofVec2f &s) {
    size = s;
    setup(getDimension(), 20);
}

ofVec2f BezierSurface::getSize() const {
    return size;
}

bool BezierSurface::showingWireFrame() const {
    return showWireFrame;
}

bool BezierSurface::showingControls() const {
    return showControls;
}

void BezierSurface::setShowControls(bool show) {
    showControls = show;
}

void BezierSurface::setShowWireFrame(bool show) {
    showWireFrame = show;
}

void BezierSurface::setPosition(const ofVec3f &p) {
    if (pos != p) {
        pos = p;
        createSurface();
    }
}

int BezierSurface::getDimension(){
    return cx+1;
}

void BezierSurface::setDimension(int dim) {
    int oldcx = cx;
    int oldcy = cy;

    cx = dim-1;
    cy = dim-1;

    vector<ofVec3f> oldPoints(points);
    loadDefaultPoints();

    // load old points
    for (int i = 0; i <= oldcx && oldcx <= cx;i++) {
        for (int j=0; j <= oldcy && oldcy <= cy;j++) {
            points[i * cy + j] = oldPoints[i * oldcy + j];
        }
    }

    createSurface();
}

void BezierSurface::loadDefaultPoints() {
    points.clear();
    for (int i=0;i<=cx;i++) {
        for (int j=0;j<=cy;j++) {
            points.push_back(ofVec3f(ofMap(i, 0, cx, 0, size.x), ofMap(j, 0, cy, 0, size.y), 0));
        }
    }
}

void BezierSurface::reset() {
    loadDefaultPoints();
    createSurface();
}

//----------------------------------------------------- bezier.
void BezierSurface::createSurface() {
    calculateSurface(points, verts, cx, cy, rx, ry);

    for (int i = 0; i < verts.size(); i++) {
        mesh.setVertex(i, verts[i]);
    }
}

void BezierSurface::calculateSurface(vector<ofVec3f> &ip, vector<ofVec3f> &op, int cpx, int cpy, int rpx, int rpy ){
    double mui,muj;
    double bi, bj;
    float x,y,z;

    // calculate bezier surface
    for (int i=0;i<rpx;i++) {
        mui = i / (double)(rpx-1);
        for (int j=0;j<rpy;j++) {
            muj = j / (double)(rpy-1);

            op[i * rpy + j].x = pos.x;
            op[i * rpy + j].y = pos.y;
            op[i * rpy + j].z = pos.z;

            for (int ki=0;ki<=cpx;ki++) {
                bi = bezierBlend(ki,mui,cpx);
                for (int kj=0;kj<=cpy;kj++) {
                    bj = bezierBlend(kj,muj,cpy);
                    op[i * rpy + j].x += (ip[ki * cpy + kj].x * bi * bj);
                    op[i * rpy + j].y += (ip[ki * cpy + kj].y * bi * bj);
                    op[i * rpy + j].z += (ip[ki * cpy + kj].z * bi * bj);
                }
            }
        }
    }
}

double BezierSurface::bezierBlend(int k, double mu, int n) {
    int nn,kn,nkn;
    double blend=1;

    nn = n;
    kn = k;
    nkn = n - k;

    while (nn >= 1) {
        blend *= nn;
        nn--;
        if (kn > 1) {
            blend /= (double)kn;
            kn--;
        }
        if (nkn > 1) {
            blend /= (double)nkn;
            nkn--;
        }
    }
    if (k > 0)
        blend *= pow(mu,(double)k);
    if (n-k > 0)
        blend *= pow(1-mu,(double)(n-k));

    return(blend);
}

//
//  Curve.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-15.
//

#include "Curve.hpp"

Curve::Curve() {
    showControls = false;
    points.push_back(ofVec2f(-200, 0));
    points.push_back(ofVec2f(-300, -300));
    points.push_back(ofVec2f(400, -300));
    points.push_back(ofVec2f(300, 0));
    catmullRom = false;
    resolution = 120;
}

Curve::Curve(Curve const &other) : AVector(other) {
    showControls = false;
    setPoints(other.getPoints());
    setCatmullRom(other.isCatmullRom());
    resolution = other.getResolution();
}

const std::vector<ofVec2f> &Curve::getPoints() const {
    return points;
}

ofVec2f     const &Curve::getPoint(int position) const {
    return points[position];
}

int       Curve::getResolution() const {
    return resolution;
}

void    Curve::setPoints(const std::vector<ofVec2f> &pointsVector) {
    points = pointsVector;
}

void    Curve::setPoint(const ofVec2f &point, int position) {
    points[position] = point;
}

void    Curve::addPoint(const ofVec2f &point) {
    points.push_back(point);
}

void    Curve::removePoint() {
    points.pop_back();
}

void    Curve::setResolution(int res) {
    resolution = res;
}

void    Curve::setCatmullRom(bool boolean) {
    catmullRom = boolean;
}

bool    Curve::isCatmullRom() const {
    return catmullRom;
}

ofPoint Curve::getBezierPoint(std::vector<ofVec2f> &points, float t ) {
    std::vector<ofPoint> tmp;
    
    for (ofVec2f vec : points) {
        tmp.push_back(ofPoint(vec.x + pos.x, vec.y + pos.y));
    }
    
    int i = points.size() - 1;
    
    while (i > 0) {
        for (int k = 0; k < i; k++)
            tmp[k] = tmp[k] + t * ( tmp[k+1] - tmp[k] );
        i--;
    }
    return tmp[0];
}

void Curve::drawShape() {
    polyline.clear();
    if (showControls) this->drawControls();
    // Catmull-Rom method, uses first and last point as control, 2 and 3rd as begin and ends
    if (catmullRom) {
        for (ofVec2f point : points) {
            polyline.curveTo(point.x + pos.x, point.y + pos.y, resolution);
        }
    } else {
    // Bezier method
        for (int i = 1; i <= resolution; i++){
            float t = (float)i / (float)(resolution);
            polyline.addVertex(getBezierPoint(points, t));
        }
    }
    polyline.draw();
}

void Curve::drawControls() {
    ofSetColor(200,200,200);
    for (ofVec3f vec : points) {
        ofFill();
        ofDrawCircle(vec.x + pos.x, vec.y + pos.y, 5);
    }
}

void Curve::drawCursorShape(ofVec2f &position) {}

bool    Curve::showingControls() const {
    return showControls;
}

void    Curve::setShowControls(bool boolean) {
    showControls = boolean;
}

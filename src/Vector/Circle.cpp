//
//  Circle.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-14.
//

#include "Circle.hpp"

Circle::Circle() {
    radius = 10;
    resolution = 30;
    pos.x = 0;
    pos.y = 0;
}

Circle::Circle(Circle const &other) : AVector(other) {
    radius = other.getRadius();
    resolution = other.getResolution();
}

void Circle::setRadius(float r) {
    radius = r;
}

float Circle::getRadius() const {
    return radius;
}

int   Circle::getResolution() const {
    return resolution;
}

void  Circle::setResolution(int res) {
    resolution = res;
}

void Circle::drawShape() {
    ofSetCircleResolution(resolution);
    ofDrawCircle(pos, radius);
}

void Circle::drawCursorShape(ofVec2f &position) {
    ofSetCircleResolution(resolution);
    ofDrawCircle(position, 5);
}

//
//  Arc.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-03.
//

#ifndef Arc_hpp
#define Arc_hpp

#include "AVector.h"

class Arc : public AVector {
    public:
        Arc();
        Arc(Arc const &);
        void drawShape();
        void drawCursorShape(ofVec2f &position);
        const ofVec2f  &getRadius() const;
        int         getAngleBegin() const;
        int         getAngleEnd() const;
        int         getResolution() const;
        void        setRadius(const ofVec2f&);
        void        setAngleBegin(int);
        void        setAngleEnd(int);
        void        setResolution(int);
    private:
        ofPolyline      polyline;
        ofVec2f         radius;
        int             angleBegin;
        int             angleEnd;
        int             resolution;
};

#endif /* Arc_hpp */

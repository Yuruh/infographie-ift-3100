//
//  AVector.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#include "AVector.h"

AVector::AVector() {
    lineWidth = 1;
    filled = true;
    contour = true;
}

AVector::AVector(const AVector &other) {
    this->setColor(other.getColor());
    this->setContourColor(other.getContourColor());
    this->setPosition(other.getPosition());
    this->setFilled(other.isFilled());
    this->setLineWidth(other.getLineWidth());
    this->setContour(other.hasContour());
}

void AVector::draw() {
  if (this->isSelected())
  {
    color = color.getInverted();
    contourColor = contourColor.getInverted();
  }
    if (filled) {
        ofFill();
        ofSetColor(color);
        drawShape();
    }
    if (contour) {
        ofNoFill();
        ofSetLineWidth(lineWidth);
        ofSetColor(contourColor);
        drawShape();
    }
    if (this->isSelected())
    {
      color = color.getInverted();
      contourColor = contourColor.getInverted();
    }
}

void    AVector::draw(int x, int y) {
    pos.x = x;
    pos.y = y;
    draw();
}

void    AVector::setPosition(const ofVec2f &p) {
    pos.set(p);
}

void    AVector::setFilled(bool f) {
    filled = f;
}

void    AVector::setContour(bool c) {
    contour = c;
}

void    AVector::setLineWidth(float lw) {
    lineWidth = lw;
}

void    AVector::setColor(const ofColor &c) {
    color.set(c);
}

void    AVector::setContourColor(const ofColor &c) {
    contourColor.set(c);
}

ofVec2f     AVector::getPosition() const {
    return pos;
}

ofVec3f     AVector::get3DPosition() const {
  ofVec3f pos;

  pos.x = this->getPosition().x;
  pos.y = this->getPosition().y;
  pos.z = 0;
  
  return pos;
}

bool        AVector::isFilled() const {
    return filled;
}

bool        AVector::hasContour() const {
    return contour;
}

float       AVector::getLineWidth() const {
    return lineWidth;
}

ofColor     AVector::getColor() const {
    return color;
}

ofColor     AVector::getContourColor() const {
    return contourColor;
}

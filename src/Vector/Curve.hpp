//
//  Curve.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-15.
//

#ifndef Curve_hpp
#define Curve_hpp

#include "AVector.h"

class Curve : public AVector {
public:
    Curve();
    Curve(Curve const &);
    void drawShape();
    void drawCursorShape(ofVec2f &position);
    const ofVec2f  &getPoint(int) const;
    const std::vector<ofVec2f> &getPoints() const;
    int         getResolution() const;
    void        setPoint(const ofVec2f&, int);
    void        setResolution(int);
    void        setPoints(const std::vector<ofVec2f>&);
    void        addPoint(const ofVec2f &point);
    void        removePoint();
    void        setCatmullRom(bool boolean);
    bool        isCatmullRom() const;
    void        drawControls();
    
    bool    showingControls() const;
    void    setShowControls(bool);
private:
    ofPolyline              polyline;
    std::vector<ofVec2f>    points;
    bool    catmullRom;
    bool    showControls;
    int     resolution;
    ofPoint getBezierPoint(std::vector<ofVec2f> &, float);
};

#endif /* Curve_hpp */

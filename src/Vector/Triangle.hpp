//
//  Triangle.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-22.
//

#ifndef Triangle_hpp
#define Triangle_hpp

#include <array>
#include "AVector.h"

class Triangle : public AVector {
    public:
        Triangle();
        Triangle(Triangle const &);
        void drawShape();
        void drawCursorShape(ofVec2f &position);
        std::array<ofVec2f, 3>  getPoints() const;
        ofVec2f     &getPoint(int);
        void        setPoint(int, const ofVec2f&);
    private:
        std::array<ofVec2f, 3>         points;
        float           radius;
};

#endif /* Triangle_hpp */

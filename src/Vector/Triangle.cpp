//
//  Triangle.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-22.
//

#include "Triangle.hpp"

Triangle::Triangle() {
    points[0].x = 0;
    points[0].y = 10;
    points[1].x = 10;
    points[1].y = 0;
    points[2].x = 20;
    points[2].y = 10;
    pos.x = 0;
    pos.y = 0;
}

Triangle::Triangle(Triangle const &other) : AVector(other) {
    points = other.getPoints();
}

std::array<ofVec2f, 3>  Triangle::getPoints() const {
    return points;
}

ofVec2f     &Triangle::getPoint(int nb) {
    return points[nb];
}

void Triangle::drawShape() {
    ofDrawTriangle(points[0] + pos, points[1] + pos, points[2] + pos);
}

void Triangle::drawCursorShape(ofVec2f &position) {
    ofDrawTriangle(points[0] / 2 + position, points[1] / 2 + position, points[2] / 2 + position);
}

void        Triangle::setPoint(int nb, const ofVec2f &newPoint) {
    points[nb] = newPoint;
}

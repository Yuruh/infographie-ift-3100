//
//  Cursor.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-15.
//

#include "GuiCursor.hpp"

GuiCursor::GuiCursor() {
    shape = NULL;
}

void GuiCursor::drawCursorShape(ofVec2f &position) {
    
}

void GuiCursor::drawShape() {
    if (shape) {
        ofFill();
        shape->drawCursorShape(pos);
    }
}

void GuiCursor::changeShape(AVector *newShape) {
    ofHideCursor();
    shape = newShape;
}

void GuiCursor::resetShape() {
    ofShowCursor();
    shape = NULL;
}

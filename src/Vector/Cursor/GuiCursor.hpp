//
//  Cursor.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-15.
//

#ifndef GuiCursor_hpp
#define GuiCursor_hpp

#include <stdio.h>
#include "AVector.h"

class GuiCursor : public AVector {
    public:
        GuiCursor();
        void drawShape();
        void drawCursorShape(ofVec2f &position);
        void changeShape(AVector*);
        void resetShape();
    private:
        AVector     *shape;
};

#endif /* GuiCursor_hpp */

//
//  Square.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-17.
//

#include "Square.hpp"

Square::Square() {
    size.x = 10;
    size.y = 10;
    pos.x = 0;
    pos.y = 0;
}

Square::Square(Square const &other) : AVector(other) {
    rect = other.getRect();
    setSize(other.getSize());
}

void Square::drawShape() {
    rect.setPosition(pos.x, pos.y);
    rect.setSize(size.x, size.y);
    ofDrawRectangle(rect);
}

void Square::drawCursorShape(ofVec2f &position) {
    rect.setPosition(position.x, position.y);
    rect.setSize(5, 5);
    ofDrawRectangle(rect);
}

ofRectangle const &Square::getRect() const {
    return rect;
}

ofVec2f     Square::getSize() const {
    return size;
}

void    Square::setSize(const ofVec2f &s) {
    size.set(s);
}

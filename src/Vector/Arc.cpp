//
//  Arc.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-03.
//

#include "Arc.hpp"

Arc::Arc() {
    pos.x = 0;
    pos.y = 0;
    angleBegin = 0;
    angleEnd = 180;
    resolution = 120;
    radius.x = 100;
    radius.y = 100;
}

Arc::Arc(Arc const &other) : AVector(other) {
    setRadius(other.getRadius());
    angleBegin = other.getAngleBegin();
    angleEnd = other.getAngleEnd();
    resolution = other.getResolution();
}

ofVec2f     const &Arc::getRadius() const {
    return radius;
}

int       Arc::getAngleBegin() const {
    return angleBegin;
}

int       Arc::getAngleEnd() const {
    return angleEnd;
}

int       Arc::getResolution() const {
    return resolution;
}

void    Arc::setRadius(const ofVec2f &r) {
    radius = r;
}

void    Arc::setAngleBegin(int aB) {
    angleBegin = aB;
}

void    Arc::setAngleEnd(int aE) {
    angleEnd = aE;
}

void    Arc::setResolution(int res) {
    resolution = res;
}

void Arc::drawShape() {
    polyline.clear();
    polyline.arc(pos, radius.x, radius.y, angleBegin, angleEnd, resolution);
    polyline.draw();
}

void Arc::drawCursorShape(ofVec2f &position) {
    polyline.clear();
    polyline.arc(position, 8, 8, angleBegin, angleEnd, resolution);
    polyline.draw();
}

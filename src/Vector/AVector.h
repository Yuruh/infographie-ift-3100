//
//  AVector.h
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-14.
//

#ifndef AVector_h
#define AVector_h

#include "ofMain.h"
#include "ADrawable.hpp"

class AVector : public ADrawable {
    public:
        AVector();
        AVector(const AVector&);
        void draw();
        virtual void drawShape() = 0;
        virtual void drawCursorShape(ofVec2f &position) = 0;
        void        draw(int x, int y);
        void        setPosition(const ofVec2f &p);
        void        setFilled(bool);
        void        setContour(bool);
        void        setLineWidth(float);
        void        setColor(const ofColor &c);
        void        setContourColor(const ofColor &c);
        ofVec2f     getPosition() const;
        virtual ofVec3f     get3DPosition() const;
        bool        isFilled() const;
        bool        hasContour() const;
        float       getLineWidth() const;
        ofColor     getColor() const;
        ofColor     getContourColor() const;

    protected:
        float       lineWidth;
        bool        filled;
        bool        contour;
        ofVec2f     pos;
        ofColor     color;
        ofColor     contourColor;
};

#endif /* AVector_h */

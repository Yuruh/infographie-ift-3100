//
//  Circle.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-14.
//

#ifndef Circle_hpp
#define Circle_hpp

#include <stdio.h>
#include "AVector.h"

class Circle : public AVector {
    public:
        Circle();
        Circle(const Circle&);
        void drawShape();
        void drawCursorShape(ofVec2f &position);
        void setRadius(float r);
        float getRadius() const;
        int   getResolution() const;
        void  setResolution(int);
    private:
        float       radius;
        int         resolution;
};

#endif /* Circle_hpp */

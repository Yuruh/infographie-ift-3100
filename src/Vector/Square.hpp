//
//  Square.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-02-17.
//

#ifndef Square_hpp
#define Square_hpp

#include <stdio.h>
#include "AVector.h"

class Square : public AVector {
    public:
        Square();
        Square(Square const &);
        void drawShape();
        void drawCursorShape(ofVec2f &position);
        const ofRectangle &getRect() const;
        ofVec2f     getSize() const;
        void        setSize(const ofVec2f &s);

    private:
        ofVec2f         size;
        ofRectangle     rect;
};

#endif /* Square_hpp */

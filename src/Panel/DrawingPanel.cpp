//
//  DrawingPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#include "ofApp.h"
#include "DrawingPanel.hpp"
#include "IUpdatable.hpp"

DrawingPanel::DrawingPanel(ofEasyCam &cam) : cam(cam) {
    this->setup("Drawing", "drawing.xml", 200, 200);
    this->setPosition(ofGetWidth() - this->getWidth(), 0);

    selectedTool = NULL;

    Light *mainLight = new Light("Main Light");
    mainLight->getLight()->setPosition(0, 0, -120);
    mainLight->getLight()->setAttenuation(0.25);
    lights.push_back(mainLight);

    this->add(color.setup("Background", ofColor(50, 50, 50), ofColor(0, 0), ofColor(255, 255)));
    this->raytracingResult.allocate(ofGetWidth(), ofGetHeight(), OF_IMAGE_COLOR_ALPHA);

    this->resetToggles();
}

void DrawingPanel::resetToggles()
{
    this->clear();
    this->add(&color);

    for(auto const& drawable: drawables) {
        this->add(drawable->getToggle());
        drawable->getToggle()->addListener(this, &DrawingPanel::onObjectSelected);
      }

    for(auto const& elem: updatables) {
        this->add(elem->getToggle());
        elem->getToggle()->addListener(this, &DrawingPanel::onObjectSelected);
    }

    for(auto const& elem: lights) {
        this->add(elem->getToggle());
        elem->getToggle()->addListener(this, &DrawingPanel::onObjectSelected);
    }

    for(auto const& elem: raytraceables) {
        this->add(elem->getToggle());
        elem->getToggle()->addListener(this, &DrawingPanel::onObjectSelected);
    }

    ofxButton   *deleteSelected = new ofxButton();
    deleteSelected->setup("Delete selected");
    deleteSelected->addListener(this, &DrawingPanel::deleteSelected);
    this->add(deleteSelected);
}

void DrawingPanel::onObjectSelected(bool &toggled) {
    if (!toggled)
        return;
    ofVec3f position(0, 0, 0);
    unsigned int count = 0;

    for (auto it = this->drawables.begin(); it != this->drawables.end();++it) {
        if ((*it)->isSelected()) {
            position += (*it)->get3DPosition();
            count++;
        }
    }
    for (auto it = this->updatables.begin(); it != this->updatables.end();++it) {
        if ((*it)->isSelected()) {
            position += (*it)->get3DPosition();
            count++;
        }
    }
    for (auto it = this->lights.begin(); it != this->lights.end();++it) {
        if ((*it)->isSelected()) {
            position += (*it)->get3DPosition();
            count++;
        }
    }
    for (auto it = this->raytraceables.begin(); it != this->raytraceables.end();++it) {
        if ((*it)->isSelected()) {
            position += (*it)->get3DPosition();
            count++;
        }
    }


    position.z += 600 * count;
    if (count > 0)
        cam.setPosition(position / count);
}

void DrawingPanel::draw() {
    ofBackground(this->color);

    cam.begin();

    ofEnableLighting();
    for(auto const& light: lights) {
        light->draw();
    }

    if (ofApp::hideForExport == false) {
        if (selectedTool) {
            selectedTool->preDrawPreview();
        }
    }

    for(auto const& drawable: drawables) {
        drawable->draw();
    }

    for(auto const& elem: updatables) {
        elem->draw();
    }

    if (!this->raytraceables.empty())
      this->raytracingResult.draw(-ofGetWidth() / 2, -ofGetHeight() / 2);

    if (ofApp::hideForExport == false) {
        if (selectedTool) {
            selectedTool->postDrawPreview();
        }
    }

    for(auto const& light: lights) {
        light->getLight()->disable();
    }
    ofDisableLighting();
    cam.end();
}

void DrawingPanel::drawPanels() {
    this->setPosition(ofGetWidth() - this->getWidth(), 0);
    ofxPanel::draw();
}

void DrawingPanel::update() {
    for(auto const& elem: updatables) {
        elem->update(ofGetElapsedTimef());
    }

    if (selectedTool) {
        // TODO THIS Fails selectedTool->updatePreview(ofGetElapsedTimef());
    }
}

void DrawingPanel::useTool() {
    if (selectedTool) {
        selectedTool->useTool(this);
        this->resetToggles();
    }
}

void DrawingPanel::selectTool(DrawingTool *tool) {
    selectedTool = tool;
}

void DrawingPanel::deleteSelected() {
    for (auto it = this->drawables.begin(); it != this->drawables.end();) {
        if ((*it)->isSelected())
            it = this->drawables.erase(it);
        else
            ++it;
    }
    for (auto it = this->updatables.begin(); it != this->updatables.end();) {
        if ((*it)->isSelected())
            it = this->updatables.erase(it);
        else
            ++it;
    }
    for (auto it = this->lights.begin(); it != this->lights.end();) {
        if ((*it)->isSelected())
            it = this->lights.erase(it);
        else
            ++it;
    }
    for (auto it = this->raytraceables.begin(); it != this->raytraceables.end();) {
        if ((*it)->isSelected())
        {
            it = this->raytraceables.erase(it);
            this->raytrace();
          }
        else
            ++it;
    }

    this->resetToggles();
}

void DrawingPanel::addDrawable(IDrawable *newObj) {
    drawables.push_back(newObj);
}

void DrawingPanel::addUpdatable(IUpdatable *newObj) {
    updatables.push_back(newObj);
}

void DrawingPanel::addLight(Light *newObj) {
    lights.push_back(newObj);
}

void DrawingPanel::raytrace()
{
  if (!this->raytraceables.empty())
  {
    this->raytraceables[0]->setCameraPos(cam.getPosition());
    this->raytraceables[0]->setOtherRaytraceableObjects(&this->raytraceables);
    this->raytraceables[0]->setLights(&this->lights);
    this->raytraceables[0]->compute(this->raytracingResult);
  }
}

void DrawingPanel::addRaytraceable(ARaytraceable *obj) {
//    std::cout << "JAJOUTE UN OBJET RAYTRACEABLE" << std::endl;

    this->raytraceables.push_back(obj);
    this->raytrace();

/*    obj->setCameraPos(cam.getPosition());
    obj->setOtherRaytraceableObjects(&this->raytraceables);
    obj->setLights(&this->lights);
    obj->compute(this->raytracingResult);*/
}

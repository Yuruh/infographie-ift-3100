//
//  ToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "ofApp.h"
#include "ToolPanel.hpp"

ToolPanel::ToolPanel(ofApp *app) : app(app) {
    this->setup("GeometryPanel", "geometryPanel.xml", 0, 0);
    this->setPosition(200, 0);
    
    ofxButton    *placeButton = new ofxButton();
    placeButton->setup("Place");
    placeButton->addListener(app, &ofApp::launchTool);
    this->add(placeButton);
}

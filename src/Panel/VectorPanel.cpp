//
//  ToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#include "ofApp.h"
#include "VectorPanel.hpp"

VectorPanel::VectorPanel(ofApp *app, AVector *shape) : ToolPanel(app), shape(shape) {
    this->add(position.setup("Position", shape->getPosition(), ofVec2f(-ofGetWidth() / 2, -ofGetHeight() / 2), ofVec2f(ofGetWidth() / 2, ofGetHeight() / 2)));
    this->add(color.setup("Color", shape->getContourColor(), ofColor(0, 0), ofColor(255, 255)));
}

void VectorPanel::update() {
    this->shape->setPosition(position);
    this->shape->setContourColor(color);
}

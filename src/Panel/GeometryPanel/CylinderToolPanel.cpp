#include "ofApp.h"
#include "CylinderToolPanel.hpp"

CylinderToolPanel::CylinderToolPanel(ofApp *app, Cylinder *shape) : GeometryPanel(app, shape), cylinder(shape) {
/*  this->raytraceBtn = new ofxButton();
  this->raytraceBtn->setup("Raytrace");
  this->raytraceBtn->addListener(this, &cylinderToolPanel::raytrace);
  this->add(this->raytraceBtn);*/
  this->add(radius.setup("Radius", cylinder->getRadius(), 0, 1000));

  this->add(reflectSlider.setup("Reflection", 0, 0, 1));
  this->add(refractSlider.setup("Refraction", 0, 0, 1));
}

void CylinderToolPanel::raytrace()
{
  cylinder->raytrace(true);
}

void CylinderToolPanel::update() {
    GeometryPanel::update();
    cylinder->setRadius(radius.getParameter().cast<float>());
    cylinder->setReflection(reflectSlider.getParameter().cast<float>());
    cylinder->setRefraction(refractSlider.getParameter().cast<float>());
}

//
//  MeshToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 11/03/2018.
//

#include "MeshToolPanel.hpp"

MeshToolPanel::MeshToolPanel(ofApp* app, Mesh* shape) : ToolPanel(app), mesh(shape) {
    
    ofxButton   *importMeshBtn = new ofxButton();
    importMeshBtn->setup("Import mesh");
    importMeshBtn->addListener(this, &MeshToolPanel::importMesh);
    this->add(importMeshBtn);
    this->add(position.setup("Position", mesh->getPosition(), ofVec3f(-ofGetWidth() / 2, -ofGetHeight() / 2, -500), ofVec3f(ofGetWidth() / 2, ofGetHeight() / 2, 500)));
    this->add(scale.setup("Scale", mesh->getScale(), ofVec3f(0, 0, 0), ofVec3f(3, 3, 3)));
    this->add(rotation.setup("Rotation", mesh->getRotation(), ofVec3f(0, 0, 0), ofVec3f(360, 360, 360)));

    resetScale.addListener(this, &MeshToolPanel::resetScaleMesh);
    this->add(resetScale.setup("Reset scale"));
}

void    MeshToolPanel::resetScaleMesh() {
    mesh->setScale(ofVec3f(0.5, 0.5, 0.5));
    scale = mesh->getScale();
}

void    MeshToolPanel::update() {
    mesh->setPosition(*(position).operator->());
    mesh->setScale(*(scale).operator->());
    mesh->setRotation(*(rotation).operator->());
}

void    MeshToolPanel::importMesh()
{
    ofFileDialogResult result = ofSystemLoadDialog("Import Mesh");
    
    std::string type[17] = {
        ".3ds", ".ase", ".dfx", ".hmp",
        ".md2", ".md3", ".md5", ".mdc",
        ".mdl", ".nff", ".ply", ".stl",
        ".x", ".lwo", ".obj", ".smd",
        ".fbx"
    };
    
    if(result.bSuccess) {
        for (int count = 0; count < 17; count++) {
            if (result.getName().find(type[count], 0) < result.getName().size())
            {
                mesh->loadMesh(result.getName(), result.getPath());
                position = mesh->getPosition();
                scale = mesh->getScale();
                rotation = mesh->getRotation();
                return;
            }
        }
        std::cerr << "Could not load mesh" << std::endl;
    }
    else
        std::cerr << "Could not load mesh" << std::endl;
}

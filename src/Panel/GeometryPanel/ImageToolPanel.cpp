//
//  ImageToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 11/03/2018.
//

#include "ofApp.h"
#include "ImageToolPanel.hpp"

ImageToolPanel::ImageToolPanel(ofApp *app, Image *shape) : ToolPanel(app), image(shape) {
    ofxButton   *importImageBtn = new ofxButton();
    importImageBtn->setup("Import image");
    importImageBtn->addListener(this, &ImageToolPanel::importImage);
    this->add(importImageBtn);
    this->add(position.setup("Position",  this->image->getPos(), ofVec2f(-ofGetWidth() / 2, -ofGetHeight() / 2), ofVec2f(ofGetWidth() / 2, ofGetHeight() / 2)));
    this->add(size.setup("Size", this->image->getSize(), ofVec2f(0, 0), ofVec2f(2000, 2000)));
    this->add(color.setup("Color", this->image->getColor(), ofColor(0, 0, 0, 0), ofColor(255, 255, 255, 255)));

    resetSize.addListener(this, &ImageToolPanel::resetSizeImage);
    this->add(resetSize.setup("Reset size"));
}

void    ImageToolPanel::resetSizeImage() {
    image->resetSize();
    size = image->getSize();
}

void    ImageToolPanel::update() {
    image->setPos(position);
    image->setSize(size);
    image->setColor(color);
}

void    ImageToolPanel::importImage() {
    ofFileDialogResult result = ofSystemLoadDialog("Import Image");
    std::string type[4] = {".png", ".jpg", ".jpeg", ".bmp"};
    
    if(result.bSuccess) {
        //        std::transform(result.getName().begin(), result.getName().end(), result.getName().begin(), ::tolower);
        for (int count = 0; count < 4; count++) {
            if (result.getName().find(type[count], 0) < result.getName().size()) {
                image->loadImage(result.getName(), result.getPath());
                position = image->getPos();
                size = image->getSize();
            }
        }
    }
}

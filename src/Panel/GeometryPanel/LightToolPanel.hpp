//
//  LightToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 23/04/2018.
//

#ifndef LightToolPanel_hpp
#define LightToolPanel_hpp

#include "ToolPanel.hpp"
#include "Light.hpp"

class ofApp;

class LightToolPanel : public ToolPanel {
public:
    LightToolPanel(ofApp*, Light*);

    virtual void    update();
    void            updateSpotlight(float, float, float);

protected:
    Light           *light;

    ofxVec3Slider   position;
    ofxVec3Slider   orientation;
    ofxFloatSlider  attenuation;
    ofxFloatSlider  spotlightCutOff;
    ofxFloatSlider  spotConcentration;

    ofxColorSlider ambientColor;
    ofxColorSlider diffuseColor;
    ofxColorSlider specularColor;

    ofxToggle       directional;
    ofxToggle       pointlight;
    ofxToggle       spotlight;
    bool            oldState[3];
};

#endif /* LightToolPanel_hpp */

//
//  MeshToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 11/03/2018.
//

#ifndef MeshToolPanel_hpp
#define MeshToolPanel_hpp

#include "ToolPanel.hpp"
#include "Mesh.hpp"

class ofApp;

class MeshToolPanel : public ToolPanel {
public:
    MeshToolPanel(ofApp*, Mesh*);
    virtual void    update();
    void            resetScaleMesh();
    void            importMesh();

protected:
    Mesh            *mesh;
    
    ofxVec3Slider   position;
    ofxVec3Slider   scale;
    ofxVec3Slider   rotation;
    ofxButton       resetScale;
};

#endif /* MeshToolPanel_hpp */

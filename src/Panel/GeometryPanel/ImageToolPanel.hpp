//
//  ImageToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 11/03/2018.
//

#ifndef ImageToolPanel_hpp
#define ImageToolPanel_hpp

#include "ToolPanel.hpp"
#include "Image.hpp"

class ofApp;

class ImageToolPanel : public ToolPanel {
public:
    ImageToolPanel(ofApp*, Image*);
    virtual void    update();
    void            importImage();
    void            resetSizeImage();

protected:
    Image           *image;

    ofxVec2Slider   position;
    ofxVec2Slider   size;
    ofxColorSlider  color;
    ofxButton       resetSize;
};

#endif /* ImageToolPanel_hpp */

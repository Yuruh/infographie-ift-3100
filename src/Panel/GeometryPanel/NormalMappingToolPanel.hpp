//
//  NormalMappingPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-29.
//

#ifndef NormalMappingPanel_hpp
#define NormalMappingPanel_hpp

#include "ToolPanel.hpp"
#include "NormalMapping.hpp"

class ofApp;

class NormalMappingToolPanel : public ToolPanel {
public:
    NormalMappingToolPanel(ofApp*, NormalMapping*);
    virtual void    update();
    void            importMapping();
    void            resetMapping();
    
protected:
    NormalMapping   *normalMapping;
    
    ofxVec3Slider   position;
    ofxVec3Slider   positionLight;
    ofxFloatSlider  intensity;
    ofxButton       resetMappingButton;
};


#endif /* NormalMappingPanel_hpp */

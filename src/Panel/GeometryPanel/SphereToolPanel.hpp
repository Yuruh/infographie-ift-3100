//
//  SphereToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef SphereToolPanel_hpp
#define SphereToolPanel_hpp

#include "GeometryPanel.hpp"
#include "Sphere.hpp"

class SphereToolPanel : public GeometryPanel {
public:
    SphereToolPanel(ofApp*, Sphere *shape);
    void update();
    void raytrace(const void *sender, bool &toggled);

protected:
    Sphere          *sphere;
    ofxFloatSlider   radius;
    ofxToggle       raytraceToggle;
    ofxFloatSlider  reflectSlider;
    ofxFloatSlider  refractSlider;
};

#endif /* SphereToolPanel_hpp */

//
//  NormalMappingPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-29.
//

#include "NormalMappingToolPanel.hpp"

NormalMappingToolPanel::NormalMappingToolPanel(ofApp *app, NormalMapping *shape) : ToolPanel(app), normalMapping(shape) {
    ofxButton   *importMappingBtn = new ofxButton();
    importMappingBtn->setup("Import mapping");
    importMappingBtn->addListener(this, &NormalMappingToolPanel::importMapping);
    this->add(importMappingBtn);
    this->add(position.setup("Position",  this->normalMapping->get3DPosition(), ofVec3f(-ofGetWidth() / 2, -ofGetHeight() / 2, -500), ofVec3f(ofGetWidth() / 2, ofGetHeight() / 2, 500)));
    this->add(positionLight.setup("Light Position",  this->normalMapping->getLightPosition(), ofVec3f(-1, -1, -1), ofVec3f(1, 1, 1)));
    this->add(intensity.setup("Intensity", this->normalMapping->getIntensity(), 0, 100));
    
    resetMappingButton.addListener(this, &NormalMappingToolPanel::resetMapping);
    this->add(resetMappingButton.setup("Reset mapping"));
}

void    NormalMappingToolPanel::resetMapping() {
    normalMapping->resetMapping();
}

void    NormalMappingToolPanel::update() {
    normalMapping->setPosition(position);
    normalMapping->setIntensity(intensity);
    normalMapping->setLightPosition(positionLight);
}

void    NormalMappingToolPanel::importMapping() {
    ofFileDialogResult result = ofSystemLoadDialog("Import Mapping");
    std::string type[4] = {".png", ".jpg", ".jpeg", ".bmp"};
    
    if(result.bSuccess) {
        for (int count = 0; count < 4; count++) {
            if (result.getName().find(type[count], 0) < result.getName().size()) {
                normalMapping->loadMapFile(result.getPath());
            }
        }
    }
}

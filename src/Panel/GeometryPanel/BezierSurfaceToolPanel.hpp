//
//  BezierSurfaceToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef BezierSurfaceToolPanel_hpp
#define BezierSurfaceToolPanel_hpp

#include "GeometryPanel.hpp"
#include "BezierSurface.hpp"

class BezierSurfaceToolPanel : public GeometryPanel {
public:
    BezierSurfaceToolPanel(ofApp*, BezierSurface *shape);
    void update();
    void updateSurface(ofVec3f&);
    void updateDimension(int&);
    void updateSize(ofVec2f &);
protected:
    std::vector<ofParameter<ofVec3f>*>   controlPoints;
    ofParameter<int>    dimension;
    ofParameter<int>    resolution;
    ofParameter<ofVec2f>    size;
    ofxToggle      wireFrame;
    ofxToggle      controls;

    ofxGuiGroup    tmpControlGroup;
    ofxGuiGroup    *controlPointsGroup;
    BezierSurface  *surface;
private:
    void initControlPoints();
};

#endif /* BezierSurfaceToolPanel_hpp */

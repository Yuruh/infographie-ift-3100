//
//  BezierSurfaceToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "BezierSurfaceToolPanel.hpp"

BezierSurfaceToolPanel::BezierSurfaceToolPanel(ofApp *app, BezierSurface *shape) : GeometryPanel(app, shape), surface(shape) {
    this->add(wireFrame.setup("Show Wireframe", surface->showingWireFrame()));
    this->add(controls.setup("Show Control Points", surface->showingControls()));
    this->add(dimension.set("Dimension", surface->getDimension(), 2, 4));
    dimension.addListener(this, &BezierSurfaceToolPanel::updateDimension);
    this->add(size.set("Size", surface->getSize(), ofVec2f(0, 0), ofVec2f(ofGetWidth(), ofGetHeight())));
    size.addListener(this, &BezierSurfaceToolPanel::updateSize);
    controlPointsGroup = tmpControlGroup.setup("Control Points");
    this->initControlPoints();
    this->add(controlPointsGroup);
}

void BezierSurfaceToolPanel::initControlPoints() {
    int index = 0;
    
    for (ofVec3f point : surface->getPoints()) {
        ofParameter<ofVec3f> *v3Param = new ofParameter<ofVec3f>();
        controlPoints.push_back(v3Param);
        controlPointsGroup->add(v3Param->set("Control Point " + std::to_string(++index), point, ofVec3f(-ofGetWidth(), -ofGetHeight(), -500), ofVec3f(ofGetWidth(), ofGetHeight(), 500)));
        v3Param->addListener(this, &BezierSurfaceToolPanel::updateSurface);
    }
}

void BezierSurfaceToolPanel::update() {
    GeometryPanel::update();
    
    int index = 0;
    for (ofParameter<ofVec3f>* param : controlPoints) {
        surface->setPoint(*(param->operator->()), index++);
    }
    surface->setShowControls(controls);
    surface->setShowWireFrame(wireFrame);
}

void BezierSurfaceToolPanel::updateSurface(ofVec3f &vec) {
    surface->createSurface();
}

void BezierSurfaceToolPanel::updateDimension(int &dimension) {
    if (surface->getDimension() != dimension) {
        surface->setDimension(dimension);
        int index = 0;
     
        controlPointsGroup->clear();
        controlPoints.clear();
        this->initControlPoints();
    }
}

void BezierSurfaceToolPanel::updateSize(ofVec2f &size) {
    if (surface->getSize() != size) {
        surface->setSize(size);
        std::vector<ofVec3f> tmp = surface->getPoints();
        int index = 0;
        for (ofParameter<ofVec3f>* param : controlPoints) {
            param->set(tmp[index++]);
        }
    }
}

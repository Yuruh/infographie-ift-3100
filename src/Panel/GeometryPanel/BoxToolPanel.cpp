//
//  BoxToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "ofApp.h"
#include "BoxToolPanel.hpp"

BoxToolPanel::BoxToolPanel(ofApp *app, Box *shape) : GeometryPanel(app, shape), box(shape) {
    this->add(width.setup("Width", box->getWidth(), 0, 1000));
    this->add(height.setup("Height", box->getHeight(), 0, 1000));
    this->add(depth.setup("Depth", box->getDepth(), 0, 1000));
}

void BoxToolPanel::update() {
    GeometryPanel::update();
    
    box->setWidth(width.getParameter().cast<float>());
    box->setHeight(height.getParameter().cast<float>());
    box->setDepth(depth.getParameter().cast<float>());
}

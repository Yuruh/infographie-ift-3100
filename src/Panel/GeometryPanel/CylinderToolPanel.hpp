#pragma once

#include "GeometryPanel.hpp"
#include "Cylinder.hpp"

class CylinderToolPanel : public GeometryPanel {
public:
    CylinderToolPanel(ofApp*, Cylinder *shape);
    void update();
    void raytrace();

protected:
    Cylinder          *cylinder;
    ofxFloatSlider   radius;
    ofxButton       *raytraceBtn;
    ofxFloatSlider  reflectSlider;
    ofxFloatSlider  refractSlider;
};

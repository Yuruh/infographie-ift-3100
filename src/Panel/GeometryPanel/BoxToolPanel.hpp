//
//  BoxToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef BoxToolPanel_hpp
#define BoxToolPanel_hpp

#include "GeometryPanel.hpp"
#include "Box.hpp"

class BoxToolPanel : public GeometryPanel {
public:
    BoxToolPanel(ofApp*, Box *shape);
    void update();
protected:
    Box             *box;
    ofxFloatSlider  width;
    ofxFloatSlider  height;
    ofxFloatSlider  depth;
};

#endif /* BoxToolPanel_hpp */

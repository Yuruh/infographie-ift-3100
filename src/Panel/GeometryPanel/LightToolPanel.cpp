//
//  LightToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Baptiste Cadoux on 23/04/2018.
//

#include "ofApp.h"
#include "LightToolPanel.hpp"

LightToolPanel::LightToolPanel(ofApp *app, Light *light) : ToolPanel(app), light(light) {
    this->add(directional.setup("Directional", false));
    this->add(pointlight.setup("Pointlight", true));
    this->add(spotlight.setup("Spotlight", false));

    this->add(position.setup("Position",  this->light->getLight()->getPosition(),  ofVec3f(-1000, -1000, -1000),  ofVec3f(1000, 1000, 1000)));
    this->add(orientation.setup("Orientation", ofVec3f(0, 0, 0),  ofVec3f(-180, -180, -180),  ofVec3f(180, 180, 180)));
   
    this->add(spotlightCutOff.setup("Spotlight cut off", this->light->getLight()->getSpotlightCutOff(),  0, 200));
    this->add(spotConcentration.setup("Spot contentration", this->light->getLight()->getSpotConcentration(),  0, 200));
    this->add(attenuation.setup("Attenuation", 0.25,  0, 1));

    this->add(ambientColor.setup("Ambient color", this->light->getLight()->getAmbientColor(), ofFloatColor(0, 0, 0, 0), ofFloatColor(255, 255, 255, 255)));
    this->add(diffuseColor.setup("Diffuse color", this->light->getLight()->getDiffuseColor(), ofFloatColor(0, 0, 0, 0), ofFloatColor(255, 255, 255, 255)));
    this->add(specularColor.setup("Specular color", this->light->getLight()->getSpecularColor(), ofFloatColor(0, 0, 0, 0), ofFloatColor(255, 255, 255, 255)));
}

void    LightToolPanel::update() {
    if (directional != oldState[0]) {
        light->getLight()->setDirectional();
        oldState[1] = pointlight = false;
        oldState[2] = spotlight = false;
    }
    if (pointlight != oldState[1]) {
        oldState[0] = directional = false;
        light->getLight()->setPointLight();
        oldState[2] = spotlight = false;
    }
    if (spotlight != oldState[2]) {
        oldState[0] = directional = false;
        oldState[1] = pointlight = false;
        light->getLight()->setSpotlight();
    }
    oldState[0] = directional;
    oldState[1] = pointlight;
    oldState[2] = spotlight;

    light->getLight()->setSpotlightCutOff(spotlightCutOff);
    light->getLight()->setSpotConcentration(spotConcentration);
    light->getLight()->setAttenuation(attenuation);

    light->getLight()->setPosition(position);
    light->getLight()->setOrientation(orientation);

    light->getLight()->setAmbientColor(ofColor(ambientColor));
    light->getLight()->setDiffuseColor(ofColor(diffuseColor));
    light->getLight()->setSpecularColor(ofColor(specularColor));

    light->setLightDirectional(directional);
    light->setLightPointlight(pointlight);
    light->setLightSpotlight(spotlight);
    
    light->setLightAttenuation(attenuation);
    
}

//
//  SphereToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "ofApp.h"
#include "SphereToolPanel.hpp"

SphereToolPanel::SphereToolPanel(ofApp *app, Sphere *shape) : GeometryPanel(app, shape), sphere(shape) {
  // this->raytraceBtn = new ofxButton();
  // this->raytraceBtn->setup("Raytrace");
  // this->raytraceBtn->addListener(this, &SphereToolPanel::raytrace);
  this->add(this->raytraceToggle.setup("Raytrace", false));
  this->raytraceToggle.addListener(this, &SphereToolPanel::raytrace);


  this->add(radius.setup("Radius", sphere->getRadius(), 0, 1000));

  this->add(reflectSlider.setup("Reflection", 0, 0, 1));
  this->add(refractSlider.setup("Refraction", 0, 0, 1));
}

void SphereToolPanel::raytrace(const void *sender, bool &toggled)
{
  sphere->raytrace(toggled);
}

void SphereToolPanel::update() {
    GeometryPanel::update();
    sphere->setRadius(radius.getParameter().cast<float>());
    sphere->setReflection(reflectSlider.getParameter().cast<float>());
    sphere->setRefraction(refractSlider.getParameter().cast<float>());
}

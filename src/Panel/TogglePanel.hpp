//
//  TogglePanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#ifndef TogglePanel_hpp
#define TogglePanel_hpp

#include <stdio.h>
#include "ofxGui.h"
#include "ToolToggle.hpp"
#include "DrawingTool.hpp"
#include "BezierSurface.hpp"
#include "AVector.h"
#include "VectorPanel.hpp"

class ofApp;

class TogglePanel : public ofxPanel {
    public:
        TogglePanel(ofApp *app);
        void toolSelected(const void *sender, bool &toggled);
        void draw();

    protected:
        std::map<std::string, DrawingTool*>  tools;
        std::map<std::string, ToolToggle*>  toolToggles;
        ofApp   *app;
        ToolToggle  *currentToggle;

  //      ofxToggle                           cameraOrthographic;
  //      ofxToggle                           cameraPerspective;


  //      void onOrthographicSelected(const void *sender, bool &toggled);
  //      void onPerspectiveSelected(const void *sender, bool &toggled);

        bool mouseMoved(ofMouseEventArgs &args);
        bool mouseDragged(ofMouseEventArgs &args);
};

#endif /* TogglePanel_hpp */

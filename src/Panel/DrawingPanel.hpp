//
//  DrawingPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#ifndef DrawingPanel_hpp
#define DrawingPanel_hpp

#include <stdio.h>
#include <iostream>
#include "ofxGui.h"
//#include "IDrawable.hpp"

#include "IUpdatable.hpp"
#include "Image.hpp"
#include "ImageToolPanel.hpp"
#include "DrawingTool.hpp"
#include "GuiCursor.hpp"
#include "Mesh.hpp"
#include "ARaytraceable.hpp"
#include "Light.hpp"
#include "IDrawObserver.hpp"

class DrawingPanel : public ofxPanel, public IDrawObserver {
public:
    DrawingPanel(ofEasyCam &cam);
    void selectTool(DrawingTool *tool);
    void draw();
    void drawPanels();
    void update();
    void onObjectSelected(bool &);
    void resetToggles();
    void useTool();
    void raytrace();

    void addDrawable(IDrawable*);
    void addUpdatable(IUpdatable*);
    void addLight(Light*);
    void addRaytraceable(ARaytraceable*);


protected:
    //todo change for unordered_map for perf and easy retrieve
    std::vector<IDrawable*>               drawables;
    std::vector<IUpdatable*>              updatables;
    std::vector<Light*>                   lights;
    std::vector<ARaytraceable*>           raytraceables;

    ofImage                               raytracingResult;

    std::vector<ofxToggle>                seletedItems;

    DrawingTool                           *selectedTool;

    ofxColorSlider                        color;

    ofEasyCam &cam;

    ofShader        shaderBlur;

    void deleteSelected();

};

#endif /* DrawingPanel_hpp */

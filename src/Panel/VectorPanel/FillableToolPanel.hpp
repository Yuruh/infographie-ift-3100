//
//  FillableToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-30.
//

#ifndef FillableToolPanel_hpp
#define FillableToolPanel_hpp

#include "VectorPanel.hpp"

class ofApp;

class FillableToolPanel : public VectorPanel {
public:
    FillableToolPanel(ofApp*, AVector*);
    virtual void update();
protected:
    ofxToggle       filled;
    ofxToggle       contour;
    ofxColorSlider  fillColor;
    ofxFloatSlider  lineWidth;
};

#endif /* FillableToolPanel_hpp */

//
//  CurveToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-15.
//

#ifndef CurveToolPanel_hpp
#define CurveToolPanel_hpp

#include "VectorPanel.hpp"
#include "Curve.hpp"

class CurveToolPanel : public VectorPanel {
public:
    CurveToolPanel(ofApp*, Curve *shape);
    void update();
protected:
    std::vector<ofxVec2Slider*>  controlPoints;
    ofxIntSlider   resolution;
    ofxToggle      catmullRom;
    ofxToggle      controls;
    ofxGuiGroup    tmpControlGroup;
    ofxGuiGroup    *controlPointsGroup;
    Curve          *curve;
private:
    void addControlPoint();
    void removeControlPoint();
};

#endif /* CurveToolPanel_hpp */

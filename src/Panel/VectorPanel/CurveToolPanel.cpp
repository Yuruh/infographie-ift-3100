//
//  CurveToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-15.
//

#include "CurveToolPanel.hpp"

CurveToolPanel::CurveToolPanel(ofApp *app, Curve *shape) : VectorPanel(app, shape) {
    curve = shape;

    ofxButton    *addControlButton = new ofxButton();
    addControlButton->setup("Add Control");
    addControlButton->addListener(this, &CurveToolPanel::addControlPoint);
    this->add(addControlButton);
    
    ofxButton    *removeControlButton = new ofxButton();
    removeControlButton->setup("Remove Control");
    removeControlButton->addListener(this, &CurveToolPanel::removeControlPoint);
    this->add(removeControlButton);
    
    this->add(catmullRom.setup("Use tangents", curve->isCatmullRom()));
    this->add(resolution.setup("Resolution", curve->getResolution(), 1, 150));
    this->add(controls.setup("Show Control Points", curve->showingControls()));
    
    controlPointsGroup = tmpControlGroup.setup("Control Points");
    int index = 0;
    
    for (ofVec2f point : curve->getPoints()) {
        ofxVec2Slider *slider = new ofxVec2Slider();
        controlPoints.push_back(slider);
        controlPointsGroup->add(slider->setup("Control Point " + std::to_string(++index), point, ofVec2f(-ofGetWidth(), -ofGetHeight()), ofVec2f(ofGetWidth(), ofGetHeight())));
    }
    this->add(controlPointsGroup);
}

void CurveToolPanel::update() {
    VectorPanel::update();
    
    int index = 0;
    for (ofxVec2Slider* slider : controlPoints) {
        curve->setPoint(*(slider->operator->()), index++);
    }
    curve->setShowControls(controls);
    curve->setCatmullRom(catmullRom);
    curve->setResolution(resolution);
}

void CurveToolPanel::addControlPoint() {
    if (controlPoints.size() < 20) {
        ofxVec2Slider *slider = new ofxVec2Slider();

        curve->addPoint(ofVec2f(0, 0));
        controlPoints.push_back(slider);
        controlPointsGroup->add(slider->setup("Control Point " + std::to_string(controlPoints.size()), ofVec2f(0, 0), ofVec2f(-ofGetWidth(), -ofGetHeight()), ofVec2f(ofGetWidth(), ofGetHeight())));
    }
}

void CurveToolPanel::removeControlPoint() {
    if (controlPoints.size() > 2) {
        controlPoints.pop_back();
        curve->removePoint();
        controlPointsGroup->clear();
        for (ofxVec2Slider *slider : controlPoints) {
            controlPointsGroup->add(slider);
        }
    }
}

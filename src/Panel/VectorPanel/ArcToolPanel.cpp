//
//  ArcToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-11.
//

#include "ArcToolPanel.hpp"

ArcToolPanel::ArcToolPanel(ofApp *app, Arc *shape) : VectorPanel(app, shape) {
    arc = shape;
    this->add(radius.setup("Radius", ofVec2f(-500, -500), ofVec2f(-500, -500), ofVec2f(500, 500)));
    this->add(angleBegin.setup("Angle Begin", 0, 0, 360));
    this->add(angleEnd.setup("Angle End", 0, 0, 360));
    this->add(resolution.setup("Resolution", 1, 1, 150));
    
    radius.operator=(arc->getRadius());
    angleBegin.operator=(arc->getAngleBegin());
    angleEnd.operator=(arc->getAngleEnd());
    resolution.operator=(arc->getResolution());
}

void ArcToolPanel::update() {
    VectorPanel::update();
    
    arc->setRadius(*(radius.operator->()));
    arc->setAngleBegin(angleBegin.getParameter().cast<int>());
    arc->setAngleEnd(angleEnd.getParameter().cast<int>());
    arc->setResolution(resolution.getParameter().cast<int>());
}

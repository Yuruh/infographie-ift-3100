//
//  CircleToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-11.
//

#include "CircleToolPanel.hpp"

CircleToolPanel::CircleToolPanel(ofApp *app, Circle *shape) : FillableToolPanel(app, shape), circle(shape) {
    this->add(radius.setup("Radius", circle->getRadius(), 0, 2000));
    this->add(circleResolution.setup("Resolution", circle->getResolution(), 1, 150));
}

void CircleToolPanel::update() {
    FillableToolPanel::update();
    
    circle->setRadius(radius);
    circle->setResolution(circleResolution);
}

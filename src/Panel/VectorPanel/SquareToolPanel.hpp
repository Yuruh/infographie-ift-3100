//
//  SquareToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#ifndef SquareToolPanel_hpp
#define SquareToolPanel_hpp

#include "FillableToolPanel.hpp"
#include "Square.hpp"

class SquareToolPanel : public FillableToolPanel {
    public:
        SquareToolPanel(ofApp*, Square *shape);
        void update();
    protected:
        ofxVec2Slider   size;
        Square          *square;
};

#endif /* SquareToolPanel_hpp */

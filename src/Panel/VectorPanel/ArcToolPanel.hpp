//
//  ArcToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-11.
//

#ifndef ArcToolPanel_hpp
#define ArcToolPanel_hpp

#include "VectorPanel.hpp"
#include "Arc.hpp"

class ArcToolPanel : public VectorPanel {
    public:
        ArcToolPanel(ofApp*, Arc *shape);
        void update();
    protected:
        ofxVec2Slider  radius;
        ofxIntSlider   angleBegin;
        ofxIntSlider   angleEnd;
        ofxIntSlider   resolution;
        Arc            *arc;
};

#endif /* ArcToolPanel_hpp */

//
//  FillableToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-30.
//

#include "FillableToolPanel.hpp"
#include "ofApp.h"

FillableToolPanel::FillableToolPanel(ofApp *app, AVector *shape) : VectorPanel(app, shape) {
    this->add(fillColor.setup("Fill Color", shape->getColor(), ofColor(0, 0), ofColor(255, 255)));
    this->add(filled.setup("Filled", shape->isFilled()));
    this->add(contour.setup("Contour", shape->hasContour()));
    this->add(lineWidth.setup("Line width", shape->getLineWidth(), 1, 8));
}

void FillableToolPanel::update() {
    VectorPanel::update();

    this->shape->setColor(fillColor);
    this->shape->setFilled(filled);
    this->shape->setContour(contour);
    this->shape->setLineWidth(lineWidth);
}

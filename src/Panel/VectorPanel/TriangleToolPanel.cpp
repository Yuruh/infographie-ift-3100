//
//  TriangleToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-11.
//

#include "TriangleToolPanel.hpp"

TriangleToolPanel::TriangleToolPanel(ofApp *app, Triangle *shape) : FillableToolPanel(app, shape) {
    triangle = shape;
    this->add(point1.setup("Point 1", ofPoint(0, 0), ofPoint(0, 0), ofPoint(100, 100)));
    this->add(point2.setup("Point 2", ofPoint(0, 0), ofPoint(0, 0), ofPoint(100, 100)));
    this->add(point3.setup("Point 3", ofPoint(0, 0), ofPoint(0, 0), ofPoint(100, 100)));
    
    point1.operator=(triangle->getPoint(0));
    point2.operator=(triangle->getPoint(1));
    point3.operator=(triangle->getPoint(2));
}

void TriangleToolPanel::update() {
    FillableToolPanel::update();
    
    triangle->setPoint(0, *(point1.operator->()));
    triangle->setPoint(1, *(point2.operator->()));
    triangle->setPoint(2, *(point3.operator->()));
}

//
//  TriangleToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-11.
//

#ifndef TriangleToolPanel_hpp
#define TriangleToolPanel_hpp

#include "FillableToolPanel.hpp"
#include "Triangle.hpp"

class TriangleToolPanel : public FillableToolPanel {
    public:
        TriangleToolPanel(ofApp*, Triangle *shape);
        void update();
    protected:
        Triangle          *triangle;
        ofxVec2Slider     point1;
        ofxVec2Slider     point2;
        ofxVec2Slider     point3;
};

#endif /* TriangleToolPanel_hpp */

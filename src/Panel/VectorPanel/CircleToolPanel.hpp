//
//  CircleToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-11.
//

#ifndef CircleToolPanel_hpp
#define CircleToolPanel_hpp

#include "FillableToolPanel.hpp"
#include "Circle.hpp"

class CircleToolPanel : public FillableToolPanel {
    public:
        CircleToolPanel(ofApp*, Circle *shape);
        void update();
    protected:
        Circle          *circle;
        ofxFloatSlider   radius;
        ofxIntSlider     circleResolution;
};

#endif /* CircleToolPanel_hpp */

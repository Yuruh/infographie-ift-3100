//
//  SquareToolPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#include "SquareToolPanel.hpp"

SquareToolPanel::SquareToolPanel(ofApp *app, Square *shape) : FillableToolPanel(app, shape) {
    square = shape;
    this->add(size.setup("Size", ofVec2f(0, 0), ofVec2f(0, 0), ofVec2f(ofGetWidth(), ofGetHeight())));
    
    size.operator=(square->getSize());
}

void SquareToolPanel::update() {
    FillableToolPanel::update();
    
    square->setSize(*(size.operator->()));
}

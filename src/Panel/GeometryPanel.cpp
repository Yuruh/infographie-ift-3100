//
//  GeometryPanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#include "ofApp.h"
#include "GeometryPanel.hpp"

GeometryPanel::GeometryPanel(ofApp *app, AGeometry *shape) : ToolPanel(app), shape(shape) {
    this->add(position.setup("Position", shape->get3DPosition(), ofVec3f(-ofGetWidth() / 2, - ofGetHeight() / 2, -1000), ofVec3f(ofGetWidth() / 2, ofGetHeight() / 2, 1000)));
    this->add(diffuse.setup("Diffuse Color", shape->getDiffuseColor(), ofColor(0, 0), ofColor(255, 255)));
    this->add(ambient.setup("Ambient Color", shape->getAmbientColor(), ofColor(0, 0), ofColor(255, 255)));
    this->add(specular.setup("Specular Color", shape->getSpecularColor(), ofColor(0, 0), ofColor(255, 255)));
    this->add(emissive.setup("Emissive Color", shape->getEmissiveColor(), ofColor(0, 0), ofColor(255, 255)));
    this->add(shininess.setup("Shininess", shape->getShininess(), 0.0f, 1));
}

void GeometryPanel::update() {
    this->shape->setPosition(position);
    this->shape->setColors(diffuse, ambient, specular, emissive);
    this->shape->setShininess(shininess);
}

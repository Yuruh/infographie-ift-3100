//
//  TogglePanel.cpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#include "TogglePanel.hpp"
#include "ofApp.h"
#include "CircleToolPanel.hpp"
#include "CurveToolPanel.hpp"
#include "SquareToolPanel.hpp"
#include "ArcToolPanel.hpp"
#include "TriangleToolPanel.hpp"
#include "SphereToolPanel.hpp"
#include "BoxToolPanel.hpp"
#include "ImageToolPanel.hpp"
#include "MeshToolPanel.hpp"
#include "BezierSurfaceToolPanel.hpp"
#include "NormalMappingToolPanel.hpp"
#include "LightToolPanel.hpp"
#include "LightTool.hpp"
#include "VectorTool.hpp"
#include "MeshTool.hpp"
#include "SphereTool.hpp"
#include "CylinderToolPanel.hpp"

TogglePanel::TogglePanel(ofApp *app) : app(app) {
    this->setup("Toggles", "toggles.xml", 0, 0);

    tools["Circle"] = new VectorTool<Circle, CircleToolPanel>(app);
    tools["Square"] = new VectorTool<Square, SquareToolPanel>(app);
    tools["Triangle"] = new VectorTool<Triangle, TriangleToolPanel>(app);
    tools["Arc"] = new VectorTool<Arc, ArcToolPanel>(app);
    tools["Curve"] = new ShapeTool<Curve, CurveToolPanel>(app);
    tools["Sphere"] = new SphereTool<Sphere, SphereToolPanel>(app);
    tools["Cylinder"] = new SphereTool<Cylinder, CylinderToolPanel>(app);
    tools["Box"] = new ShapeTool<Box, BoxToolPanel>(app);
    tools["Bezier Surface"] = new ShapeTool<BezierSurface, BezierSurfaceToolPanel>(app);
    tools["Image"] = new ShapeTool<Image, ImageToolPanel>(app);
    tools["Mesh"] = new MeshTool<Mesh, MeshToolPanel>(app);
    tools["Light"] = new LightTool<Light, LightToolPanel>(app);
    tools["Normal Mapping"] = new ShapeTool<NormalMapping, NormalMappingToolPanel>(app);
    currentToggle = NULL;

    //  This adds toggles for all tools
    for(auto const& tool: tools) {
        ofLog() << "Toggle created for : " << tool.first;
        toolToggles[tool.first] = new ToolToggle(tool.second);
        toolToggles[tool.first]->addListener(this, &TogglePanel::toolSelected);
        this->add(toolToggles[tool.first]->setup(tool.first, false));
    }
}

void TogglePanel::draw()
{
    ofxPanel::draw();
}

void TogglePanel::toolSelected(const void *sender, bool &toggled) {
    ToolToggle  *toggle = NULL;
    DrawingTool  *tool = NULL;

    //disable current toggle
    if (currentToggle != NULL)
      currentToggle->ofxToggle::operator=(false);
    for (auto & kv : toolToggles) {
        if (*(kv.second))
        {
          toggle = kv.second;
          tool = toggle->getTool();
          break;
        }
    }

    if (toggled && tool) {
        currentToggle = toggle;
        ToolPanel   &toolPanel = tool->getPanel();
        AVector     *cursorShape = tool->getCursorShape();

        if (cursorShape) app->changeCursorShape(cursorShape);
        app->changeToolPanel(&toolPanel);
        app->changeTool(tool);
    } else {
        app->changeCursorShape(NULL);
        app->changeToolPanel(NULL);
        app->changeTool(NULL);
        currentToggle = NULL;
    }
  }

bool TogglePanel::mouseMoved(ofMouseEventArgs &args){
    app->moveCursor(args.x, args.y);
    return true;
}

bool TogglePanel::mouseDragged(ofMouseEventArgs &args){
    app->moveCursor(args.x, args.y);
    return true;
}

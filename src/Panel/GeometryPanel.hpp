//
//  GeometryPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef GeometryPanel_hpp
#define GeometryPanel_hpp

#include "ofxGui.h"
#include "Sphere.hpp"
#include "ToolPanel.hpp"

class ofApp;

class GeometryPanel : public ToolPanel {
public:
    GeometryPanel(ofApp*, AGeometry*);
    virtual void update();
protected:
    AGeometry       *shape;
    ofxFloatSlider  shininess;
    ofxVec3Slider   position;
    ofxColorSlider  diffuse;
    ofxColorSlider  ambient;
    ofxColorSlider  specular;
    ofxColorSlider  emissive;
};

#endif /* GeometryPanel_hpp */

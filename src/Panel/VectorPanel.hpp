//
//  VectorPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-03-10.
//

#ifndef VectorPanel_hpp
#define VectorPanel_hpp

#include "ofxGui.h"
#include "AVector.h"
#include "ToolPanel.hpp"

class ofApp;

class VectorPanel : public ToolPanel {
    public:
        VectorPanel(ofApp*, AVector*);
        virtual void update();
    protected:
        AVector         *shape;
        ofxVec2Slider   position;
        ofxColorSlider  color;
};

#endif /* VectorPanel_hpp */

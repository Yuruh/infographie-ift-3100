//
//  ToolPanel.hpp
//  infographie-ift-3100
//
//  Created by Raphael Thiebault on 2018-04-22.
//

#ifndef ToolPanel_hpp
#define ToolPanel_hpp

#include "ofxGui.h"

class ofApp;

class ToolPanel : public ofxPanel {
    public:
        ToolPanel(ofApp*);
        virtual void update() = 0;
    protected:
        ofApp   *app;
};

#endif /* ToolPanel_hpp */

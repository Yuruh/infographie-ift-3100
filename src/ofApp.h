#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "AVector.h"
#include "Image.hpp"
#include "TogglePanel.hpp"
#include "ToolPanel.hpp"
#include "DrawingPanel.hpp"
#include "Sphere.hpp"
#include "Cylinder.hpp"

class ofApp : public ofBaseApp {

public:
    ofApp();
    void setup();
    void update();
    void draw();

    void exit();

    // from ToolPanel
    void launchTool();

    // from TogglePanel
    void changeCursorShape(AVector *);
    void changeTool(DrawingTool*);
    void changeToolPanel(ToolPanel*);
    void activateTool(bool);

    void exportImage();
    void load3dModel();

    void keyPressed(int key);
    void moveCursor(int x, int y);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void changeCameraProjection();

    GuiCursor       mouseCursor;

    static bool    hideForExport;

    TogglePanel     togglePanel;
    DrawingPanel    drawingPanel;
    ToolPanel       *toolPanel;

    ofEasyCam	      cam;

    bool            isOrtho;
};

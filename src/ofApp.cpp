#include "ofApp.h"
#include "Mesh.hpp"

bool    ofApp::hideForExport = false;

ofApp::ofApp() : ofBaseApp(), togglePanel(this), drawingPanel(cam) {}

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetLogLevel(OF_LOG_VERBOSE);
    ofSetFrameRate(60);
    ofSetVerticalSync(true);

    toolPanel = NULL;
    ofSetGlobalAmbientColor(0.3);

    ofxButton   *exportImageBtn = new ofxButton();
    exportImageBtn->setup("Export image");
    exportImageBtn->addListener(this, &ofApp::exportImage);
    togglePanel.add(exportImageBtn);

    ofxButton   *changeCameraProjection = new ofxButton();
    changeCameraProjection->setup("Change Projection");
    changeCameraProjection->addListener(this, &ofApp::changeCameraProjection);
    togglePanel.add(changeCameraProjection);

    cam.disableOrtho();
    isOrtho = false;
}

//--------------------------------------------------------------
void ofApp::exit() {
    //    TODO : clear tout ca
}

void ofApp::changeCursorShape(AVector *shape) {
    (shape) ? mouseCursor.changeShape(shape) : mouseCursor.resetShape();
}

void ofApp::changeTool(DrawingTool *tool) {
    drawingPanel.selectTool(tool);
}

void ofApp::changeToolPanel(ToolPanel *panel) {
    toolPanel = panel;
}

void ofApp::launchTool() {
    drawingPanel.useTool();
}

void ofApp::exportImage() {
    hideForExport = true;
    draw();

    ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + ".png", "Export image to PNG");
    if (saveFileResult.bSuccess){
        ofImage         exportImg;

        exportImg.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
        exportImg.save(saveFileResult.filePath);
    }
    hideForExport = false;
}

void ofApp::changeCameraProjection() {
  if (isOrtho) {
    cam.disableOrtho();
  }
  else
    cam.enableOrtho();
  isOrtho = !isOrtho;
}

//--------------------------------------------------------------
void ofApp::update() {
    drawingPanel.update();
    if (toolPanel) {
        toolPanel->update();
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofSetColor(255,255,255,255);

    drawingPanel.draw();
    if (ofApp::hideForExport == false) {

        drawingPanel.drawPanels();
        togglePanel.draw();
        if (toolPanel) toolPanel->draw();
        mouseCursor.drawShape();
    }
}

void ofApp::moveCursor(int x, int y) {
    ofVec2f vec(x, y);

    mouseCursor.setPosition(vec);
}

void ofApp::mouseMoved(int x, int y){
    moveCursor(x, y);
}

void ofApp::mouseDragged(int x, int y, int button){
    moveCursor(x, y);
}

void ofApp::keyPressed(int key){
  if (key == 'q')
  {
    ofVec3f pos = cam.getPosition();
    pos.z -= 10;
    cam.setPosition(pos);
  }

  if (key == 'e')
  {
    ofVec3f pos = cam.getPosition();
    pos.z += 10;
    cam.setPosition(pos);
  }

  if (key == OF_KEY_LEFT || key == 'a')
  {
    ofVec3f pos = cam.getPosition();
    pos.x -= 10;
    cam.setPosition(pos);
  }

  if (key == OF_KEY_RIGHT || key == 'd')
  {
    ofVec3f pos = cam.getPosition();
    pos.x += 10;
    cam.setPosition(pos);
  }

  if (key == OF_KEY_UP || key == 'w')
  {
    ofVec3f pos = cam.getPosition();
    pos.y += 10;
    cam.setPosition(pos);
  }
  if (key == OF_KEY_DOWN || key == 's')
  {
    ofVec3f pos = cam.getPosition();
    pos.y -= 10;
    cam.setPosition(pos);
  }

}

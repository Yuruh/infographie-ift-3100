# Infographie

## Prerequis

1.  OpenFrameworks, son ProjectGenerator et ses dépendances doivent être installés.
2.  Importez ce dossier dans le ProjectGenerator et lancez la génération
3.  En fonction de votre OS, buildez le projet
    -   Sur MacOS, lancez le projet a travers le .xcodeproj
4. Lancez l'exécutable généré
5. Amusez-vous :)
